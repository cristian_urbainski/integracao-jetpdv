package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.pdv.domain.vendedor.PdvDadosVendedorDTO;
import br.com.viasoft.integracaojetpdv.pdv.service.AbstractPdvDefaultService;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.util.ListUtil;
import org.springframework.stereotype.Service;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
@Service
public class PdvPessoaVendedorServiceImpl
        extends AbstractPdvDefaultService<PessoaVendedor, PdvDadosVendedorDTO>
        implements PdvPessoaVendedorService {

    @Override
    protected boolean noDataReturned(PdvDadosVendedorDTO dto) {

        return dto == null || ListUtil.isEmpty(dto.getVendedores());
    }

    @Override
    protected RotinaSincronizacao getRotinaSincronizacao() {

        return RotinaSincronizacao.VENDEDOR;
    }

}
