package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.pessoa.Pessoa;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@Entity
@Table(name = "PESSOA_VENDEDOR")
@Getter
@Setter
public class PessoaVendedor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_VENDEDOR")
    private Long id;

    @JsonBackReference
    @JoinColumn(name = "ID_PESSOA")
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    private Pessoa pessoa;

}
