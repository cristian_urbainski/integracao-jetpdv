package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.pdv.controller.AbstractPdvDefaultController;
import br.com.viasoft.integracaojetpdv.pdv.controller.IPdvController;
import br.com.viasoft.security.annotation.AuthenticationType;
import br.com.viasoft.security.annotation.Protected;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
@RestController
@RequestMapping("/vendedores")
@Protected(authenticationType = AuthenticationType.PRIVATE)
public class PdvPessoaVendedorControllerImpl
        extends AbstractPdvDefaultController<PessoaVendedor>
        implements IPdvController {

}
