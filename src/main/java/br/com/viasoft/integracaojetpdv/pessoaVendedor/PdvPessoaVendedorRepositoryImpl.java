package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.controlesincronizacao.ControleSincronizacao;
import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.integracaojetpdv.pdv.repository.AbstractPdvDefaultRepository;
import br.com.viasoft.integracaojetpdv.pessoa.Pessoa;
import br.com.viasoft.integracaojetpdv.pessoa.Pessoa_;
import br.com.viasoft.jdbc.exception.WarningException;
import br.com.viasoft.rest.util.MessageUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
@Repository
public class PdvPessoaVendedorRepositoryImpl
        extends AbstractPdvDefaultRepository<PessoaVendedor>
        implements PdvPessoaVendedorRepository {

    @Override
    protected void addJoins(Root<PessoaVendedor> from) {
        super.addJoins(from);

        from.join(PessoaVendedor_.pessoa, JoinType.LEFT);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Predicate mountDataHoraUltimaAlteracaoPredicate(
            CriteriaBuilder cb, Root<PessoaVendedor> from, ControleSincronizacao controleSincronizacao) {

        var ref = new Object() {
            Join<PessoaVendedor, Pessoa> joinPessoa;
        };

        from.getJoins().forEach(join -> {
            if (join.getJavaType().equals(Pessoa.class)) {
                ref.joinPessoa = (Join<PessoaVendedor, Pessoa>) join;
            }
        });

        if (ref.joinPessoa == null) {
            throw new WarningException(MessageUtil.get(I18NProperties.NAO_FOI_POSSIVEL_CARREGAR_DADOS_DO_VENDEDOR));
        }

        return cb.greaterThanOrEqualTo(
                ref.joinPessoa.get(Pessoa_.dataHoraUltimaAlteracao), controleSincronizacao.getDataHoraSincronizacao());
    }

}
