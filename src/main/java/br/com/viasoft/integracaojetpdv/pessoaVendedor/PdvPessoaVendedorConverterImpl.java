package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.integracaojetpdv.pdv.converter.AbstractPdvDadosDTOConverter;
import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.Record;
import br.com.viasoft.integracaojetpdv.pdv.domain.vendedor.PdvDadosVendedorDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.vendedor.VendedorDTO;
import br.com.viasoft.integracaojetpdv.pessoa.Pessoa;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacaolog.StatusSincronizacaoLog;
import br.com.viasoft.rest.util.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
@Slf4j
@Component
public class PdvPessoaVendedorConverterImpl
        extends AbstractPdvDadosDTOConverter<PessoaVendedor, PdvDadosVendedorDTO>
        implements PdvPessoaVendedorConverter {

    @Override
    protected boolean isValidEntity(PessoaVendedor pessoaVendedor) {
        return pessoaVendedor != null && pessoaVendedor.getPessoa() != null;
    }

    @Override
    protected void saveSincronizacaoLogs(Sincronizacao sincronizacao, PessoaVendedor pessoaVendedor) {

        if (pessoaVendedor == null) {
            log.error(MessageUtil.get(I18NProperties.VENDEDOR_NAO_ENCONTRADO));
            return;
        }

        if (pessoaVendedor.getPessoa() == null) {

            sincronizacaoLogService.save(
                    sincronizacao,
                    pessoaVendedor.getId(),
                    MessageUtil.get(I18NProperties.VENDEDOR_SEM_PESSOA),
                    StatusSincronizacaoLog.ERRO);
        }
    }

    @Override
    protected void convertEntity(PessoaVendedor pessoaVendedor, PdvDadosVendedorDTO to) {

        var vendedorDto = VendedorDTO.builder()
                .record(Record.VENDEDOR)
                .idVendedor(pessoaVendedor.getId())
                .name(StringUtils.substring(pessoaVendedor.getPessoa().getNome(), 0, 50))
                .nrCartao("0")
                .valorComissao(BigDecimal.ZERO)
                .status(getStatus(pessoaVendedor.getPessoa()))
                .build();

        to.addVendedor(vendedorDto);
    }

    @Override
    protected RotinaSincronizacao getRotinaSincronizacao() {

        return RotinaSincronizacao.VENDEDOR;
    }

    private AtivoInativo getStatus(Pessoa pessoa) {

        return pessoa == null || pessoa.getInativo() ? AtivoInativo.INATIVO : AtivoInativo.ATIVO;
    }

}
