package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.pdv.converter.IConverter;
import br.com.viasoft.integracaojetpdv.pdv.converter.PdvDadosDTOConverterData;
import br.com.viasoft.integracaojetpdv.pdv.domain.vendedor.PdvDadosVendedorDTO;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
public interface PdvPessoaVendedorConverter extends IConverter<PdvDadosDTOConverterData<PessoaVendedor>, PdvDadosVendedorDTO> {

}
