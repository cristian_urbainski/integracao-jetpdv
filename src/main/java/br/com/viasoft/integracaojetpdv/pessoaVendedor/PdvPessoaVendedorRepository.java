package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.pdv.repository.IPdvRepository;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
public interface PdvPessoaVendedorRepository extends IPdvRepository<PessoaVendedor> {
}
