package br.com.viasoft.integracaojetpdv.pessoaVendedor;

import br.com.viasoft.integracaojetpdv.pdv.service.IPdvService;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
public interface PdvPessoaVendedorService extends IPdvService<PessoaVendedor> {
}
