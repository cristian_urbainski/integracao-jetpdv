package br.com.viasoft.integracaojetpdv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableCircuitBreaker
@EnableDiscoveryClient
@ComponentScan("br.com.viasoft.*")
public class IntegracaoJetpdvApplication {

	public static void main(String[] args) {

		SpringApplication.run(IntegracaoJetpdvApplication.class, args);
	}

}
