package br.com.viasoft.integracaojetpdv.sincronizacaolog;

import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Entity
@Table(name = "SINCRONIZACAO_LOG")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SincronizacaoLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SINCRONIZACAO_LOG")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SINCRONIZACAO")
    private Sincronizacao sincronizacao;

    @Column(name = "ID_REGISTRO")
    private Long idRegistro;

    @Column(name = "MENSAGEM")
    private String mensangem;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private StatusSincronizacaoLog status;

    @Column(name = "IDENTIFICADOR_REGISTRO", length = 36)
    private String identificadorRegistro;

}
