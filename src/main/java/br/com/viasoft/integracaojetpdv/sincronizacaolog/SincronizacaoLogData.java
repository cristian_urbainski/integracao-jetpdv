package br.com.viasoft.integracaojetpdv.sincronizacaolog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface SincronizacaoLogData extends JpaRepository<SincronizacaoLog, Long> {

    @Query(value = """
        SELECT count(*) FROM SINCRONIZACAO_LOG AS SL
        WHERE 
        SL.IDENTIFICADOR_REGISTRO = :uuid
    """, nativeQuery = true)
    Long countBy(@Param("uuid") String uuid);

}
