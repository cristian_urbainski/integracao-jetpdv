package br.com.viasoft.integracaojetpdv.sincronizacaolog;

import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import br.com.viasoft.jdbc.repository.JpaCrudRepository;
import br.com.viasoft.jdbc.service.impl.JpaCrudServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SincronizacaoLogServiceImpl
        extends JpaCrudServiceImpl<SincronizacaoLog, Long, Void>
        implements SincronizacaoLogService {

    private final SincronizacaoLogRepository repository;
    private final SincronizacaoLogData data;

    @Override
    protected JpaRepository<SincronizacaoLog, Long> getData() {

        return data;
    }

    @Override
    protected JpaCrudRepository<SincronizacaoLog, Void> getRepository() {

        return repository;
    }

    @Override
    public Long countBy(String uuid) {

        return data.countBy(uuid);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public SincronizacaoLog save(
            Sincronizacao sincronizacao,
            Long idRegistro,
            String mensagm,
            StatusSincronizacaoLog status) {

        var sincronizacaoLog = SincronizacaoLog.builder()
                .sincronizacao(sincronizacao)
                .identificadorRegistro(sincronizacao.getIdentificadorRegistro())
                .idRegistro(idRegistro)
                .mensangem(mensagm)
                .status(status)
                .build();

        return saveAndFlush(sincronizacaoLog);
    }

}
