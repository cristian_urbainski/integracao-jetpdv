package br.com.viasoft.integracaojetpdv.sincronizacaolog;

import br.com.viasoft.jdbc.repository.JpaCrudRepository;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface SincronizacaoLogRepository extends JpaCrudRepository<SincronizacaoLog, Void> {

}
