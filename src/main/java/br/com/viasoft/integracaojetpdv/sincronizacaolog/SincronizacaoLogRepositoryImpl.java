package br.com.viasoft.integracaojetpdv.sincronizacaolog;

import br.com.viasoft.jdbc.repository.impl.JpaCrudRepositoryImpl;
import org.springframework.stereotype.Repository;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Repository
public class SincronizacaoLogRepositoryImpl
        extends JpaCrudRepositoryImpl<SincronizacaoLog, Void>
        implements SincronizacaoLogRepository {

}
