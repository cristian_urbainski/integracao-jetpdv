package br.com.viasoft.integracaojetpdv.sincronizacaolog;

import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import br.com.viasoft.jdbc.service.JpaCrudService;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface SincronizacaoLogService extends JpaCrudService<SincronizacaoLog, Long, Void> {

    Long countBy(String uuid);

    SincronizacaoLog save(
            Sincronizacao sincronizacao,
            Long idRegistro,
            String mensagm,
            StatusSincronizacaoLog status);

}
