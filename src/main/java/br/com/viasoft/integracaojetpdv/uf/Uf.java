package br.com.viasoft.integracaojetpdv.uf;

import br.com.viasoft.integracaojetpdv.pais.Pais;
import br.com.viasoft.integracaojetpdv.timeZone.TimeZone;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "UF", catalog = "VIASOFT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Uf implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_UF")
    private Long id;

    @Size(max = 2)
    @Column(name = "UF", length = 2, unique = true)
    private String uf;

    @Size(max = 40)
    @Column(name = "NOME", length = 40)
    private String nome;

    @Size(max = 1)
    @Column(name = "NACIONAL", length = 1)
    private String nacional;

    @Column(name = "CODIGO_IBGE")
    private Integer codigoIBGE;

    @Column(name = "INATIVO")
    public Boolean inativo;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PAIS")
    private Pais pais;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TIME_ZONE")
    private TimeZone timeZone;

}
