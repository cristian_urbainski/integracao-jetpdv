package br.com.viasoft.integracaojetpdv.sincronizacao;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Entity
@Table(name = "SINCRONIZACAO")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Sincronizacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SINCRONIZACAO")
    private Long id;

    @Column(name = "ID_FILIAL")
    private Long idFilial;

    @Column(name = "NUMERO_PAGINA")
    private Integer numeroPagina;

    @Column(name = "TAMANHO_PAGINA")
    private Integer tamanhoPagina;

    @Column(name = "DATA_HORA_SINCRONIZACAO")
    private LocalDateTime dataHoraSincronizacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROTINA_SINCRONIZACAO")
    private RotinaSincronizacao rotinaSincronizacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_INTEGRACAO")
    private TipoIntegracao tipoIntegracao;

    @Column(name = "IDENTIFICADOR_REGISTRO", length = 36)
    private String identificadorRegistro;

}
