package br.com.viasoft.integracaojetpdv.sincronizacao;

import br.com.viasoft.jdbc.repository.JpaCrudRepository;
import br.com.viasoft.jdbc.service.impl.JpaCrudServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SincronizacaoServiceImpl
        extends JpaCrudServiceImpl<Sincronizacao, Long, Void>
        implements SincronizacaoService {

    private final SincronizacaoRepository repository;
    private final SincronizacaoData data;

    @Override
    protected JpaRepository<Sincronizacao, Long> getData() {

        return data;
    }

    @Override
    protected JpaCrudRepository<Sincronizacao, Void> getRepository() {

        return repository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Sincronizacao findOrCreate(
            String uuid,
            Long idFilial,
            Integer pagina,
            Integer tamanhoPagina,
            RotinaSincronizacao rotinaSincronizacao,
            TipoIntegracao tipoIntegracao) {

        var op = data.findBy(uuid);

        if (op.isPresent()) {

            return op.get();
        }

        var sincronizacao = Sincronizacao.builder()
                .idFilial(idFilial)
                .numeroPagina(pagina)
                .tamanhoPagina(tamanhoPagina)
                .rotinaSincronizacao(rotinaSincronizacao)
                .tipoIntegracao(tipoIntegracao)
                .identificadorRegistro(uuid)
                .dataHoraSincronizacao(LocalDateTime.now())
                .build();

        return data.saveAndFlush(sincronizacao);
    }

}
