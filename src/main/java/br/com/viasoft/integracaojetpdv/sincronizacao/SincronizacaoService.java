package br.com.viasoft.integracaojetpdv.sincronizacao;

import br.com.viasoft.jdbc.service.JpaCrudService;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface SincronizacaoService extends JpaCrudService<Sincronizacao, Long, Void> {

    Sincronizacao findOrCreate(
            String uuid,
            Long idFilial,
            Integer pagina,
            Integer tamanhoPagina,
            RotinaSincronizacao rotinaSincronizacao,
            TipoIntegracao tipoIntegracao);

}
