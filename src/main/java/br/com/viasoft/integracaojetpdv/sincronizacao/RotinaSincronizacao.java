package br.com.viasoft.integracaojetpdv.sincronizacao;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public enum RotinaSincronizacao {
    CLIENTE,
    PRODUTO,
    USUARIO,
    VENDEDOR
}
