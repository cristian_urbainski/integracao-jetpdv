package br.com.viasoft.integracaojetpdv.sincronizacao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface SincronizacaoData extends JpaRepository<Sincronizacao, Long> {

    @Query(value = """
        SELECT * FROM SINCRONIZACAO AS S
        WHERE 
        S.IDENTIFICADOR_REGISTRO = :uuid
    """, nativeQuery = true)
    Optional<Sincronizacao> findBy(@Param("uuid") String uuid);

}
