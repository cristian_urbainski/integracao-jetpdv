package br.com.viasoft.integracaojetpdv.sincronizacao;

import br.com.viasoft.jdbc.repository.JpaCrudRepository;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface SincronizacaoRepository extends JpaCrudRepository<Sincronizacao, Void> {

}
