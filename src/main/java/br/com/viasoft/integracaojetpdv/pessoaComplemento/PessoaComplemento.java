package br.com.viasoft.integracaojetpdv.pessoaComplemento;

import br.com.viasoft.integracaojetpdv.orgaoEmissorDocumento.OrgaoEmissorDocumento;
import br.com.viasoft.integracaojetpdv.perfilTributario.PerfilTributario;
import br.com.viasoft.integracaojetpdv.pessoa.Pessoa;
import br.com.viasoft.integracaojetpdv.pessoaEndereco.PessoaEndereco;
import br.com.viasoft.integracaojetpdv.pessoaVendedor.PessoaVendedor;
import br.com.viasoft.integracaojetpdv.uf.Uf;
import br.com.viasoft.jdbc.annotations.DataHoraUltimaAlteracaoAutomatico;
import br.com.viasoft.jdbc.entity.generic.DataHoraUltimaAlteracaoViasoftEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "PESSOA_COMPLEMENTO")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PessoaComplemento implements DataHoraUltimaAlteracaoViasoftEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_COMPLEMENTO")
    private Long id;

    @NotNull
    @JsonBackReference(value = "complemento")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PESSOA")
    private Pessoa pessoa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_RH_ORGAO_EMISSOR_DOCUMENTO")
    private OrgaoEmissorDocumento orgaoEmissorRg;

    @Column(name = "DATA_EXPEDICAO_RG")
    private LocalDate dataExpedicaoRg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_UF_RG")
    private Uf ufRg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PERFIL_TRIBUTARIO")
    private PerfilTributario perfilTributario;

    @Size(max = 60)
    @Column(name = "NOME_FANTASIA", length = 60)
    private String fantasia;

    @NotNull
    @Column(name = "INDICADOR_INSCRICAO_ESTADUAL", nullable = false)
    private Integer indicadorInscricaoEstadual;

    @Size(max = 18)
    @Column(name = "INSCRICAO_ESTADUAL", length = 18)
    private String inscricaoEstadual;

    @Size(max = 60)
    @Column(name = "EMAIL_PRINCIPAL", length = 60)
    private String emailPrincipal;

    @Size(max = 500)
    @Column(name = "EMAIL_DOCUMENTO_ELETRONICO", length = 500)
    private String emailDocumentoEletronico;

    @Size(max = 15)
    @Column(name = "INSCRICAO_MUNICIPAL", length = 15)
    private String inscricaoMunicipal;

    @Size(max = 14)
    @Column(name = "INSCRICAO_PRODUTOR", length = 14)
    private String inscricaoProdutor;

    @JsonProperty()
    @Size(max = 9)
    @Column(name = "INSCRICAO_SUFRAMA", length = 9)
    private String inscricaoSuframa;

    @Column(name = "RG", length = 15)
    private String rg;

    @Column(name = "COMPLEMENTO_PRINCIPAL", nullable = false)
    private Boolean complementoPrincipal;

    @Column(name = "AREA_PROPRIEDADE", precision = 15, scale = 2)
    private BigDecimal areaPropriedade;

    @Size(max = 60)
    @Column(name = "NOME_PROPRIEDADE", length = 60)
    private String nomePropriedade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ID_PESSOA_VENDEDOR")
    private PessoaVendedor vendedor;

    @Size(max = 255)
    @Column(name = "OBSERVACOES", length = 255)
    private String observacoes;

    @NotNull
    @Column(name = "INATIVO", nullable = false)
    private Boolean inativo;

    @DataHoraUltimaAlteracaoAutomatico
    @Column(name = "DATA_HORA_ULTIMA_ALTERACAO")
    private LocalDateTime dataHoraUltimaAlteracao;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PESSOA_COMPLEMENTO")
    private Set<PessoaEndereco> pessoaEnderecoSet = new HashSet<>();

}
