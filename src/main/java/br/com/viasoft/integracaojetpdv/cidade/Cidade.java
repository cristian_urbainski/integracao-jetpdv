package br.com.viasoft.integracaojetpdv.cidade;

import br.com.viasoft.integracaojetpdv.uf.Uf;
import lombok.*;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "CIDADE", catalog = "VIASOFT")
@Getter
@Setter
@Immutable
@NoArgsConstructor
@AllArgsConstructor
public class Cidade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CIDADE")
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_UF")
    private Uf uf;

    @Column(name = "CODIGO_IBGE")
    private Integer codigoIBGE;

}
