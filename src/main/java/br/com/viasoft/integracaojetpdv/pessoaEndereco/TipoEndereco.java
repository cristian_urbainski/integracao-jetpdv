package br.com.viasoft.integracaojetpdv.pessoaEndereco;

/**
 * @author Cristian Urbainski
 * @since 10/12/2021
 */
public enum TipoEndereco {
    PRINCIPAL("P"),
    COBRANCA("C"),
    ALTERNATIVO("A");

    TipoEndereco(String tipo) {

        this.tipo = tipo;
    }

    private final String tipo;

    public String getTipo() {

        return tipo;
    }

}
