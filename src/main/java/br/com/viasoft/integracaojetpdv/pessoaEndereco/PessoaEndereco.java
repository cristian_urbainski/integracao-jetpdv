package br.com.viasoft.integracaojetpdv.pessoaEndereco;

import br.com.viasoft.integracaojetpdv.cidade.Cidade;
import br.com.viasoft.integracaojetpdv.pessoaComplemento.PessoaComplemento;
import br.com.viasoft.jdbc.annotations.DataHoraUltimaAlteracaoAutomatico;
import br.com.viasoft.jdbc.entity.generic.DataHoraUltimaAlteracaoViasoftEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "PESSOA_ENDERECO")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PessoaEndereco implements DataHoraUltimaAlteracaoViasoftEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA_ENDERECO")
    private Long id;

    @NotNull
    @JsonBackReference
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PESSOA_COMPLEMENTO")
    private PessoaComplemento pessoaComplemento;

    @NotBlank
    @Size(max = 1)
    @Column(name = "TIPO_ENDERECO", length = 1, nullable = false)
    private String tipoEndereco;

    @Size(max = 20)
    @Column(name = "NUMERO", length = 20)
    private String numero;

    @Size(max = 8)
    @Column(name = "CEP", length = 8)
    private String cep;

    @Size(max = 60)
    @Column(name = "LOGRADOURO", length = 60)
    private String logradouro;

    @Size(max = 60)
    @Column(name = "COMPLEMENTO", length = 60)
    private String complemento;

    @Size(max = 60)
    @Column(name = "BAIRRO", length = 60)
    private String bairro;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CIDADE")
    private Cidade cidade;

    @Size(max = 16)
    @Column(name = "FONE_PRINCIPAL", length = 16)
    private String telefonePrincipal;

    @Size(max = 16)
    @Column(name = "FONE_COMERCIAL", length = 16)
    private String telefoneComercial;

    @Size(max = 2000)
    @Column(name = "REFERENCIA", length = 2000)
    private String referencia;

    @DataHoraUltimaAlteracaoAutomatico
    @Column(name = "DATA_HORA_ULTIMA_ALTERACAO")
    private LocalDateTime dataHoraUltimaAlteracao;

}
