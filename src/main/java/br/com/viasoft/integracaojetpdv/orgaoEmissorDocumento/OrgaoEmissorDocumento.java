package br.com.viasoft.integracaojetpdv.orgaoEmissorDocumento;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "RH_ORGAO_EMISSOR_DOCUMENTO", catalog = "VIASOFT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrgaoEmissorDocumento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RH_ORGAO_EMISSOR_DOCUMENTO")
    private Long id;

    @Size(max = 15)
    @Column(name = "CODIGO_ESOCIAL", length = 15)
    private String codigoEsocial;

    @Size(max = 15)
    @Column(name = "SIGLA", length = 15)
    private String sigla;

    @Size(max = 150)
    @Column(name = "DESCRICAO", length = 150)
    private String descricao;

}
