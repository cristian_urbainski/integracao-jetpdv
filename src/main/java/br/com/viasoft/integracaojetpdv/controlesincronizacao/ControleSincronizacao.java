package br.com.viasoft.integracaojetpdv.controlesincronizacao;

import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Entity
@Table(name = "CONTROLE_SINCRONIZACAO")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ControleSincronizacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONTROLE_SINCRONIZACAO")
    private Long id;

    @Column(name = "ID_FILIAL")
    private Long idFilial;

    @Column(name = "DATA_HORA_SINCRONIZACAO")
    private LocalDateTime dataHoraSincronizacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROTINA_SINCRONIZACAO")
    private RotinaSincronizacao rotinaSincronizacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_INTEGRACAO")
    private TipoIntegracao tipoIntegracao;

}
