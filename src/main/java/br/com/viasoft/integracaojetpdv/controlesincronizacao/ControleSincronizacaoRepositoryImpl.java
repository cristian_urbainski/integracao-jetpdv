package br.com.viasoft.integracaojetpdv.controlesincronizacao;

import br.com.viasoft.jdbc.repository.impl.JpaCrudRepositoryImpl;
import org.springframework.stereotype.Repository;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Repository
public class ControleSincronizacaoRepositoryImpl
        extends JpaCrudRepositoryImpl<ControleSincronizacao, Void>
        implements ControleSincronizacaRepository {

}
