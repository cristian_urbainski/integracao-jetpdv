package br.com.viasoft.integracaojetpdv.controlesincronizacao;

import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import br.com.viasoft.jdbc.repository.JpaCrudRepository;
import br.com.viasoft.jdbc.service.impl.JpaCrudServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ControleSincronizacaServiceImpl
        extends JpaCrudServiceImpl<ControleSincronizacao, Long, Void>
        implements ControleSincronizacaService {

    private final ControleSincronizacaRepository repository;
    private final ControleSincronizacaoData data;

    @Override
    protected JpaRepository<ControleSincronizacao, Long> getData() {

        return data;
    }

    @Override
    protected JpaCrudRepository<ControleSincronizacao, Void> getRepository() {

        return repository;
    }

    @Override
    public Optional<ControleSincronizacao> findBy(
            Long idFilial,
            RotinaSincronizacao rotinaSincronizacao,
            TipoIntegracao tipoIntegracao) {

        return data.findBy(idFilial, rotinaSincronizacao, tipoIntegracao);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createOrUpdate(
            Long idFilial,
            RotinaSincronizacao rotinaSincronizacao,
            TipoIntegracao tipoIntegracao) {

        var op = data.findBy(idFilial, rotinaSincronizacao, tipoIntegracao);

        var controleSincronizacao = op.orElse(null);

        if (controleSincronizacao == null) {

            controleSincronizacao = ControleSincronizacao.builder()
                    .idFilial(idFilial)
                    .dataHoraSincronizacao(LocalDateTime.now())
                    .rotinaSincronizacao(rotinaSincronizacao)
                    .tipoIntegracao(tipoIntegracao)
                    .build();

        } else {

            controleSincronizacao.setDataHoraSincronizacao(LocalDateTime.now());
        }

        data.saveAndFlush(controleSincronizacao);
    }

}
