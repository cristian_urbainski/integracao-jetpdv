package br.com.viasoft.integracaojetpdv.controlesincronizacao;

import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import br.com.viasoft.jdbc.service.JpaCrudService;

import java.util.Optional;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface ControleSincronizacaService extends JpaCrudService<ControleSincronizacao, Long, Void> {

    Optional<ControleSincronizacao> findBy(
            Long idFilial,
            RotinaSincronizacao rotinaSincronizacao,
            TipoIntegracao tipoIntegracao);

    void createOrUpdate(
            Long idFilial,
            RotinaSincronizacao rotinaSincronizacao,
            TipoIntegracao tipoIntegracao);

}
