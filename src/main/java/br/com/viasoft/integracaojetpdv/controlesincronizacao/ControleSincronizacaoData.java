package br.com.viasoft.integracaojetpdv.controlesincronizacao;

import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface ControleSincronizacaoData extends JpaRepository<ControleSincronizacao, Long> {

    @Query(value = """
        SELECT * FROM CONTROLE_SINCRONIZACAO CS
        WHERE 
        CS.ID_FILIAL = :idFilial AND
        CS.ROTINA_SINCRONIZACAO= :#{#rotinaSincronizacao.name()} AND 
        CS.TIPO_INTEGRACAO = :#{#tipoIntegracao.name()}
    """, nativeQuery = true)
    Optional<ControleSincronizacao> findBy(
            @Param("idFilial") Long idFilial,
            @Param("rotinaSincronizacao")RotinaSincronizacao rotinaSincronizacao,
            @Param("tipoIntegracao")TipoIntegracao tipoIntegracao);
}
