package br.com.viasoft.integracaojetpdv.controlesincronizacao;

import br.com.viasoft.jdbc.repository.JpaCrudRepository;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface ControleSincronizacaRepository extends JpaCrudRepository<ControleSincronizacao, Void> {

}
