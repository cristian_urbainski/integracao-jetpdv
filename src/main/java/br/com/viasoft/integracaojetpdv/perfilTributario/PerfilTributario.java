package br.com.viasoft.integracaojetpdv.perfilTributario;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "PERFIL_TRIBUTARIO", catalog = "VIASOFT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PerfilTributario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PERFIL_TRIBUTARIO")
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Column(name = "DESCRICAO", length = 50, nullable = false)
    private String descricao;

}
