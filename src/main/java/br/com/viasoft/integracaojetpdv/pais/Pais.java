package br.com.viasoft.integracaojetpdv.pais;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PAIS", uniqueConstraints = {@UniqueConstraint(name = "UNQ_PAIS",
        columnNames = {"PAIS"})}, catalog = "VIASOFT")
@Immutable
@Getter
@Setter
public class Pais implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PAIS")
    private Long id;

    @Column(name = "PAIS", unique = true)
    private Integer pais;

    @Column(name = "NOME", length = 100)
    private String nome;

    @Column(name = "INATIVO")
    private Boolean inativo;

    @Column(name = "SISCOMEX")
    private Integer siscomex;

    @Column(name = "SIGLA", length = 3, nullable = false)
    private String sigla;

}
