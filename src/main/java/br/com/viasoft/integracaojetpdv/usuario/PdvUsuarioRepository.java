package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pdv.repository.IPdvRepository;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
public interface PdvUsuarioRepository extends IPdvRepository<Usuario> {

}
