package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pessoa.Pessoa;
import br.com.viasoft.jdbc.annotations.DataHoraUltimaAlteracaoAutomatico;
import br.com.viasoft.jdbc.hibernateConverters.BooleanNotNullConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name="USUARIO", uniqueConstraints = {@UniqueConstraint(name = "UNQ_USUARIO", columnNames = {"CPF"})})
@Getter
@Setter
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USUARIO")
    private Long id;

    @Column(name = "ID_USUARIO_OAUTH")
    private Long idUsuarioOauth;

    @Size(max = 11)
    @Column(name="CPF", length=11, unique = true)
    private String cpf;

    @Size(max = 200)
    @Column(name="NOME", length=200)
    private String nome;

    @Size(max = 20)
    @Column(name="APELIDO", length=20)
    private String apelido;

    @Size(max = 200)
    @Column(name="EMAIL", length=200)
    private String email;

    @Size(max = 14)
    @Column(name = "SENHA_JETPDV", length = 14)
    private String senhaJetPdv;

    @Column(name = "TIPO_USUARIO")
    @Enumerated(EnumType.STRING)
    private UsuarioTipo tipo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PESSOA")
    private Pessoa pessoa;

    @Convert(converter = BooleanNotNullConverter.class)
    @Column(name = "INATIVO", nullable = false)
    private Boolean inativo;

    @DataHoraUltimaAlteracaoAutomatico
    @Column(name = "DATA_HORA_ULTIMA_ALTERACAO")
    private LocalDateTime dataHoraUltimaAlteracao;

}
