package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pdv.repository.AbstractPdvDefaultRepository;
import br.com.viasoft.integracaojetpdv.pessoa.Pessoa_;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@Repository
public class PdvUsuarioRepositoryImpl extends AbstractPdvDefaultRepository<Usuario> implements PdvUsuarioRepository {

    @Override
    protected void addJoins(Root<Usuario> from) {
        super.addJoins(from);

        from.join(Usuario_.pessoa, JoinType.LEFT).join(Pessoa_.pessoaVendedor, JoinType.LEFT);
    }

}
