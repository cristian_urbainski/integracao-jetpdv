package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pdv.converter.IConverter;
import br.com.viasoft.integracaojetpdv.pdv.converter.PdvDadosDTOConverterData;
import br.com.viasoft.integracaojetpdv.pdv.domain.usuario.PdvDadosUsuarioDTO;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
public interface PdvUsuarioConverter extends IConverter<PdvDadosDTOConverterData<Usuario>, PdvDadosUsuarioDTO> {

}
