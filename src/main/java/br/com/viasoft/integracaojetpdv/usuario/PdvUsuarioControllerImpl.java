package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pdv.controller.AbstractPdvDefaultController;
import br.com.viasoft.integracaojetpdv.pdv.controller.IPdvController;
import br.com.viasoft.security.annotation.AuthenticationType;
import br.com.viasoft.security.annotation.Protected;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@RestController
@RequestMapping("/usuarios")
@Protected(authenticationType = AuthenticationType.PRIVATE)
public class PdvUsuarioControllerImpl extends AbstractPdvDefaultController<Usuario> implements IPdvController {
}
