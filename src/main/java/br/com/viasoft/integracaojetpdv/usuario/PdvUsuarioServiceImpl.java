package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pdv.domain.usuario.PdvDadosUsuarioDTO;
import br.com.viasoft.integracaojetpdv.pdv.service.AbstractPdvDefaultService;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.util.ListUtil;
import org.springframework.stereotype.Service;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@Service
public class PdvUsuarioServiceImpl
        extends AbstractPdvDefaultService<Usuario, PdvDadosUsuarioDTO>
        implements PdvUsuarioService {

    @Override
    protected boolean noDataReturned(PdvDadosUsuarioDTO dto) {

        return dto == null || ListUtil.isEmpty(dto.getUsers());
    }

    @Override
    protected RotinaSincronizacao getRotinaSincronizacao() {

        return RotinaSincronizacao.USUARIO;
    }

}
