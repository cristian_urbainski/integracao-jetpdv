package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.pdv.service.IPdvService;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
public interface PdvUsuarioService extends IPdvService<Usuario> {

}
