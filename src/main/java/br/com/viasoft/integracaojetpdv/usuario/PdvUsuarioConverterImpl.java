package br.com.viasoft.integracaojetpdv.usuario;

import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.integracaojetpdv.pdv.converter.AbstractPdvDadosDTOConverter;
import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.Record;
import br.com.viasoft.integracaojetpdv.pdv.domain.usuario.PdvDadosUsuarioDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.usuario.UsuarioDTO;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacaolog.StatusSincronizacaoLog;
import br.com.viasoft.rest.util.MessageUtil;
import br.com.viasoft.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@Slf4j
@Component
public class PdvUsuarioConverterImpl
        extends AbstractPdvDadosDTOConverter<Usuario, PdvDadosUsuarioDTO>
        implements PdvUsuarioConverter {

    @Override
    protected boolean isValidEntity(Usuario usuario) {
        return usuario != null && usuario.getPessoa() != null && usuario.getPessoa().getPessoaVendedor() != null;
    }

    @Override
    protected void saveSincronizacaoLogs(Sincronizacao sincronizacao, Usuario usuario) {

        if (usuario == null) {
            log.error(MessageUtil.get(I18NProperties.USUARIO_NAO_ENCONTRADO));
            return;
        }

        if (usuario.getPessoa() == null || usuario.getPessoa().getPessoaVendedor() == null) {

            sincronizacaoLogService.save(
                    sincronizacao,
                    usuario.getId(),
                    MessageUtil.get(I18NProperties.USUARIO_SEM_VENDEDOR),
                    StatusSincronizacaoLog.ERRO);
        }
    }

    @Override
    protected void convertEntity(Usuario usuario, PdvDadosUsuarioDTO to) {

        var userDto = UsuarioDTO.builder()
                .record(Record.USUARIO)
                .id(usuario.getId())
                .name(usuario.getNome())
                .password(getPassword(usuario))
                .status(getStatus(usuario))
                .idVendedor(getIdVendedor(usuario))
                .build();

        to.addUser(userDto);
    }

    @Override
    protected RotinaSincronizacao getRotinaSincronizacao() {

        return RotinaSincronizacao.USUARIO;
    }

    private String getPassword(Usuario usuario) {

        if (usuario == null) {
            return null;
        }

        if (StringUtil.isEmptyOrNull(usuario.getSenhaJetPdv())) {
            return String.valueOf(usuario.getId());
        }

        return usuario.getSenhaJetPdv();
    }

    private Long getIdVendedor(Usuario usuario) {

        if (usuario == null) {
            return null;
        }

        var pessoa = usuario.getPessoa();
        if (pessoa == null) {
            return null;
        }

        var pessoaVendedor = pessoa.getPessoaVendedor();
        if (pessoaVendedor == null) {
            return null;
        }

        return pessoaVendedor.getId();
    }

    private AtivoInativo getStatus(Usuario usuario) {
        if (usuario == null) {
            return AtivoInativo.INATIVO;
        }

        return usuario.getInativo() == null || usuario.getInativo() ? AtivoInativo.INATIVO : AtivoInativo.ATIVO;
    }

}
