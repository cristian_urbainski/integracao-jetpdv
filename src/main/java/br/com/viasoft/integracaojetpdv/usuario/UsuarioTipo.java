package br.com.viasoft.integracaojetpdv.usuario;

public enum UsuarioTipo {

    S("SUPERVISOR"), N("NORMAL");

    UsuarioTipo(String tipo) {

        this.tipo = tipo;
    }

    private final String tipo;

    public String getTipo() {
        return tipo;
    }

}
