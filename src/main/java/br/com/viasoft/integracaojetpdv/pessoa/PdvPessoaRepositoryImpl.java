package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.controlesincronizacao.ControleSincronizacao;
import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.integracaojetpdv.pdv.repository.AbstractPdvDefaultRepository;
import br.com.viasoft.integracaojetpdv.pessoaComplemento.PessoaComplemento;
import br.com.viasoft.integracaojetpdv.pessoaComplemento.PessoaComplemento_;
import br.com.viasoft.integracaojetpdv.pessoaEndereco.PessoaEndereco;
import br.com.viasoft.integracaojetpdv.pessoaEndereco.PessoaEndereco_;
import br.com.viasoft.jdbc.exception.WarningException;
import br.com.viasoft.rest.util.MessageUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
@Repository
public class PdvPessoaRepositoryImpl extends AbstractPdvDefaultRepository<Pessoa> implements PdvPessoaRepository {

    @Override
    protected void addJoins(Root<Pessoa> from) {
        super.addJoins(from);

        var joinPessoaComplemento = from.join(Pessoa_.pessoaComplementoSet, JoinType.LEFT);
        joinPessoaComplemento.join(PessoaComplemento_.ufRg, JoinType.LEFT);

        var joinPessoaEndereco = joinPessoaComplemento.join(PessoaComplemento_.pessoaEnderecoSet, JoinType.LEFT);
        joinPessoaEndereco.join(PessoaEndereco_.cidade, JoinType.LEFT);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Predicate mountDataHoraUltimaAlteracaoPredicate(
            CriteriaBuilder cb,
            Root<Pessoa> from,
            ControleSincronizacao controleSincronizacao) {

        var ref = new Object() {
            Join<Pessoa, PessoaComplemento> joinPessoaComplemento;
            Join<PessoaComplemento, PessoaEndereco> joinPessoaEndereco;
        };

        from.getJoins().forEach(join -> {
            if (join.getJavaType().equals(PessoaComplemento.class)) {
                ref.joinPessoaComplemento = (Join<Pessoa, PessoaComplemento>) join;
            }
        });

        if (ref.joinPessoaComplemento == null) {
            throw new WarningException(MessageUtil.get(I18NProperties.NAO_FOI_POSSIVEL_CARREGAR_DADOS_DO_COMPLEMENTO));
        }

        ref.joinPessoaComplemento.getJoins().forEach(join -> {
            if (join.getJavaType().equals(PessoaEndereco.class)) {
                ref.joinPessoaEndereco = (Join<PessoaComplemento, PessoaEndereco>) join;
            }
        });

        if (ref.joinPessoaEndereco == null) {
            throw new WarningException(MessageUtil.get(I18NProperties.NAO_FOI_POSSIVEL_CARREGAR_DADOS_DO_ENDERECO));
        }

        var pessoaDataHoraUltimaAlteracaoPath = from.get(Pessoa_.dataHoraUltimaAlteracao);
        var pessoaComplementoDataHoraUltimaAlteracaoPath = ref.joinPessoaComplemento.get(PessoaComplemento_.dataHoraUltimaAlteracao);
        var pessoaEnderecoDataHoraUltimaAlteracaoPath = ref.joinPessoaEndereco.get(PessoaEndereco_.dataHoraUltimaAlteracao);

        var p1 = cb.greaterThanOrEqualTo(pessoaDataHoraUltimaAlteracaoPath, controleSincronizacao.getDataHoraSincronizacao());
        var p2 = cb.greaterThanOrEqualTo(pessoaComplementoDataHoraUltimaAlteracaoPath, controleSincronizacao.getDataHoraSincronizacao());
        var p3 = cb.greaterThanOrEqualTo(pessoaEnderecoDataHoraUltimaAlteracaoPath, controleSincronizacao.getDataHoraSincronizacao());

        return cb.or(p1, p2, p3);
    }

}
