package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.pessoaComplemento.PessoaComplemento;
import br.com.viasoft.integracaojetpdv.pessoaVendedor.PessoaVendedor;
import br.com.viasoft.jdbc.annotations.DataHoraUltimaAlteracaoAutomatico;
import br.com.viasoft.jdbc.hibernateConverters.BigDecimalNotNullConverter;
import br.com.viasoft.jdbc.hibernateConverters.BooleanNotNullConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@Entity
@Table(name = "PESSOA", uniqueConstraints = {@UniqueConstraint(name = "UNQ_PESSOA_01", columnNames = {"IDENTIFICACAO"})})
@Getter
@Setter
public class Pessoa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA")
    private Long id;

    @Version
    @Column(name = "VERSAO_REGISTRO")
    private Long versao;

    @NotBlank
    @Size(max = 60)
    @Column(name = "NOME", length = 60, nullable = false)
    private String nome;

    @NotBlank
    @Size(max = 14)
    @Column(name = "IDENTIFICACAO", length = 14, nullable = false, unique = true)
    private String identificacao;

    @Column(name = "EH_PESSOA_FISICA", nullable = false)
    private Boolean ehFisica;

    @Column(name = "EH_FORNECEDOR", nullable = false)
    private Boolean ehFornecedor = false;

    @Column(name = "EH_MOTORISTA", nullable = false)
    private Boolean ehMotorista = false;

    @Convert(converter = BooleanNotNullConverter.class)
    @Column(name = "EH_REVENDEDOR", nullable = false)
    private Boolean ehRevendedor = false;

    @Column(name = "EH_TRANSPORTADORA", nullable = false)
    private Boolean ehTransportadora = false;

    @Column(name = "EH_EMPREGADO", nullable = false)
    private Boolean ehEmpregado = false;

    @Convert(converter = BooleanNotNullConverter.class)
    @Column(name = "EH_RESPONSAVEL_TECNICO_REAG", nullable = false)
    private Boolean ehResponsavelTecnicoReag;

    @Convert(converter = BooleanNotNullConverter.class)
    @Column(name = "EH_VENDEDOR", nullable = false)
    private Boolean ehVendedor;

    @Convert(converter = BooleanNotNullConverter.class)
    @Column(name = "EH_CONSULENTE", nullable = false)
    private Boolean ehConsulente;

    @Convert(converter = BooleanNotNullConverter.class)
    @Column(name = "EH_CONTADOR", nullable = false)
    private Boolean ehContador;

    @Column(name = "INATIVO", nullable = false)
    private Boolean inativo;

    @DataHoraUltimaAlteracaoAutomatico
    @Column(name = "DATA_HORA_ULTIMA_ALTERACAO")
    private LocalDateTime dataHoraUltimaAlteracao;

    @Convert(converter = BigDecimalNotNullConverter.class)
    @Column(name = "VALOR_LIMITE_CREDITO", nullable = false, precision = 15, scale = 2)
    private BigDecimal valorLimiteCredito;

    @NotNull
    @Column(name = "VALOR_UTILIZADO_LIMITE_CREDITO", nullable = false, precision = 15, scale = 2)
    private BigDecimal valorUtilizadoLimiteCredito = BigDecimal.ZERO;

    @CreationTimestamp
    @Column(name = "DATA_CADASTRO")
    private LocalDate dataCadastro;

    @Column(name = "DATA_REVISAO_LIMITE_CREDITO")
    private LocalDate dataRevisaoLimiteCredito;

    @JsonManagedReference
    @OneToOne(mappedBy = "pessoa", fetch = FetchType.LAZY)
    private PessoaVendedor pessoaVendedor;

    @JsonManagedReference(value = "complemento")
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PESSOA")
    @OrderBy("complementoPrincipal DESC")
    private Set<PessoaComplemento> pessoaComplementoSet = new LinkedHashSet<>();

}
