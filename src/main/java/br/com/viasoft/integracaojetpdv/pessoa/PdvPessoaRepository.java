package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.pdv.repository.IPdvRepository;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
public interface PdvPessoaRepository extends IPdvRepository<Pessoa> {
}
