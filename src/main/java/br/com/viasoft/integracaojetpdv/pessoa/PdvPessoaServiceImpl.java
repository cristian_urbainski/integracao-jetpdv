package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.pdv.domain.cliente.PdvDadosClienteDTO;
import br.com.viasoft.integracaojetpdv.pdv.service.AbstractPdvDefaultService;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.util.ListUtil;
import org.springframework.stereotype.Service;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
@Service
public class PdvPessoaServiceImpl
        extends AbstractPdvDefaultService<Pessoa, PdvDadosClienteDTO>
        implements PdvPessoaService {

    @Override
    protected RotinaSincronizacao getRotinaSincronizacao() {
        return RotinaSincronizacao.CLIENTE;
    }

    @Override
    protected boolean noDataReturned(PdvDadosClienteDTO dto) {

        return dto == null || ListUtil.isEmpty(dto.getClientes());
    }

}
