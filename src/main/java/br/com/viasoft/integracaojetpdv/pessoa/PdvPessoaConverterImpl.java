package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.integracaojetpdv.pdv.converter.AbstractPdvDadosDTOConverter;
import br.com.viasoft.integracaojetpdv.pdv.domain.cliente.*;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.Record;
import br.com.viasoft.integracaojetpdv.pessoaComplemento.PessoaComplemento;
import br.com.viasoft.integracaojetpdv.pessoaEndereco.PessoaEndereco;
import br.com.viasoft.integracaojetpdv.pessoaEndereco.TipoEndereco;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacaolog.StatusSincronizacaoLog;
import br.com.viasoft.rest.util.MessageUtil;
import br.com.viasoft.util.DocumentoUtil;
import br.com.viasoft.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
@Slf4j
@Component
public class PdvPessoaConverterImpl
        extends AbstractPdvDadosDTOConverter<Pessoa, PdvDadosClienteDTO>
        implements PdvPessoaConverter {

    private static final int ID_RAMO_ATIVIDADE_PADRAO = 1;
    private static final int ID_CLIENTE_TIPO = 1;
    private static final int ID_CLIENTE_SITUACAO_CREDITO = 1;
    private static final int ID_CLIENTE_CONCEITO = 1;
    private static final int ID_CLIENTE_TIPO_ENDERECO = 1;
    private static final int ID_CLIENTE_TIPO_TELEFONE = 1;
    private static final int ID_TIPO_PRECO = 1;
    private static final int ID_TIPO_PRECO_EXTRA = 0;

    @Override
    protected boolean isValidEntity(Pessoa entity) {

        var valido = entity != null;

        valido = valido && DocumentoUtil.isCpfOuCnpj(entity.getIdentificacao());

        if (valido) {

            var optionalPessoaComplemento = entity.getPessoaComplementoSet()
                    .stream()
                    .filter(PessoaComplemento::getComplementoPrincipal)
                    .findFirst();

            if (optionalPessoaComplemento.isPresent()) {

                var pessoaComplemento = optionalPessoaComplemento.get();

                var optionalPessoaEnderecoPrincipal = pessoaComplemento.getPessoaEnderecoSet()
                        .stream()
                        .filter(pe -> TipoEndereco.PRINCIPAL.getTipo().equals(pe.getTipoEndereco()))
                        .findFirst();

                valido = optionalPessoaEnderecoPrincipal.isPresent();
            } else {

                valido = false;
            }
        }

        return valido;
    }

    @Override
    protected void saveSincronizacaoLogs(Sincronizacao sincronizacao, Pessoa pessoa) {

        if (pessoa == null) {
            log.error(MessageUtil.get(I18NProperties.CLIENTE_NAO_ENCONTRADO));
            return;
        }

        if (!DocumentoUtil.isCpfOuCnpj(pessoa.getIdentificacao())) {
            sincronizacaoLogService.save(
                    sincronizacao,
                    pessoa.getId(),
                    MessageUtil.get(I18NProperties.CLIENTE_CPFCNPJ_INVALIDO),
                    StatusSincronizacaoLog.ERRO);
        }

        var optionalPessoaComplemento = pessoa.getPessoaComplementoSet()
                .stream()
                .filter(PessoaComplemento::getComplementoPrincipal)
                .findFirst();

        if (optionalPessoaComplemento.isPresent()) {

            var pessoaComplemento = optionalPessoaComplemento.get();

            var optionalPessoaEnderecoPrincipal = pessoaComplemento.getPessoaEnderecoSet()
                    .stream()
                    .filter(pe -> TipoEndereco.PRINCIPAL.getTipo().equals(pe.getTipoEndereco()))
                    .findFirst();

            if (optionalPessoaEnderecoPrincipal.isEmpty()) {

                sincronizacaoLogService.save(
                        sincronizacao,
                        pessoa.getId(),
                        MessageUtil.get(I18NProperties.ENDERECO_PRINCIPAL_NAO_ENCONTRADO),
                        StatusSincronizacaoLog.ERRO);
            }
        } else {

            sincronizacaoLogService.save(
                    sincronizacao,
                    pessoa.getId(),
                    MessageUtil.get(I18NProperties.COMPLEMENTO_PRINCIPAL_NAO_ENCONTRADO),
                    StatusSincronizacaoLog.ERRO);
        }
    }

    @Override
    protected void convertEntity(Pessoa entity, PdvDadosClienteDTO dto) {

        var optionalPessoaComplemento = entity.getPessoaComplementoSet()
                .stream()
                .filter(PessoaComplemento::getComplementoPrincipal)
                .findFirst();

        if (optionalPessoaComplemento.isEmpty()) {
            return;
        }

        var pessoaComplemento = optionalPessoaComplemento.get();

        var optionalPessoaEnderecoPrincipal = pessoaComplemento.getPessoaEnderecoSet()
                    .stream()
                    .filter(pe -> TipoEndereco.PRINCIPAL.getTipo().equals(pe.getTipoEndereco()))
                    .findFirst();

        if (optionalPessoaEnderecoPrincipal.isEmpty()) {
            return;
        }

        var pessoaEnderecoPrincipal = optionalPessoaEnderecoPrincipal.get();

        buildClienteDto(entity, pessoaComplemento, dto);
        buildClienteEnderecoDto(entity, pessoaEnderecoPrincipal, dto);
        buildClienteEnderecoTelefoneDto(entity, pessoaEnderecoPrincipal, dto);
        buildClienteIdsDto(entity, dto);
    }

    @Override
    protected void converterCabecalhos(PdvDadosClienteDTO pdvDadosClienteDTO) {
        super.converterCabecalhos(pdvDadosClienteDTO);

        buildClienteRamoDto(pdvDadosClienteDTO);
        buildClienteTipoDto(pdvDadosClienteDTO);
        buildClienteSituacaoCreditoDto(pdvDadosClienteDTO);
        buildClienteTipoEnderecoDto(pdvDadosClienteDTO);
        buildClienteTipoTelefoneDto(pdvDadosClienteDTO);
        buildClienteConceitoDto(pdvDadosClienteDTO);
        buildClienteIdentificadorDto(pdvDadosClienteDTO);
    }

    @Override
    protected RotinaSincronizacao getRotinaSincronizacao() {
        return RotinaSincronizacao.CLIENTE;
    }

    private void buildClienteDto(
            Pessoa pessoa,
            PessoaComplemento pessoaComplemento,
            PdvDadosClienteDTO dto) {

        var clienteDto = ClienteDTO.builder()
                .record(Record.CLIENTE)
                .id(pessoa.getId())
                .nome(StringUtils.substring(pessoa.getNome(), 0, 60))
                .status(getStatus(pessoa))
                .tipoCliente(getTipoFisica(pessoa))
                .cpfCnpj(pessoa.getIdentificacao())
                .rgIe(pessoa.getEhFisica() ? pessoaComplemento.getRg() : pessoaComplemento.getInscricaoEstadual())
                .ufRg(pessoaComplemento.getUfRg() != null ? pessoaComplemento.getUfRg().getUf() : null)
                .valorLimiteCredito(pessoa.getValorLimiteCredito())
                .valorUtilizadoLimiteCredito(pessoa.getValorUtilizadoLimiteCredito())
                .dataCadastro(pessoa.getDataCadastro())
                .dataRevisaoLimiteCredito(pessoa.getDataRevisaoLimiteCredito())
                .nrInscricaoEstadual(StringUtils.substring(pessoaComplemento.getInscricaoEstadual(), 0, 20))
                .email(StringUtils.substring(pessoaComplemento.getEmailDocumentoEletronico(), 0, 60))
                .idClienteTipo(ID_CLIENTE_TIPO)
                .idClienteSituacaoCredito(ID_CLIENTE_SITUACAO_CREDITO)
                .idClienteConceito(ID_CLIENTE_CONCEITO)
                .build();

        dto.addCliente(clienteDto);
    }

    private void buildClienteEnderecoDto(Pessoa pessoa, PessoaEndereco pessoaEndereco, PdvDadosClienteDTO dto) {

        var clienteEnderecoDTO = ClienteEnderecoDTO.builder()
                .record(Record.CLIENTE_ENDERECO)
                .idCliente(pessoa.getId())
                .tipoEndereco(getTipoEndecoDto(pessoaEndereco))
                .cep(pessoaEndereco.getCep())
                .codigoMunicipio(StringUtil.toString(pessoaEndereco.getCidade().getCodigoIBGE()))
                .uf(pessoaEndereco.getCidade().getUf().getUf())
                .cidade(StringUtils.substring(pessoaEndereco.getCidade().getNome(), 0, 30))
                .bairro(StringUtils.substring(pessoaEndereco.getBairro(), 0, 20))
                .endereco(StringUtils.substring(pessoaEndereco.getLogradouro(), 0, 60))
                .nrEndereco(StringUtils.substring(pessoaEndereco.getNumero(), 0, 6))
                .complemento(pessoaEndereco.getComplemento())
                .idClienteTipoEndereco(ID_CLIENTE_TIPO_ENDERECO)
                .build();

        dto.addEndereco(clienteEnderecoDTO);
    }

    private void buildClienteEnderecoTelefoneDto(Pessoa pessoa, PessoaEndereco pessoaEndereco, PdvDadosClienteDTO dto) {

        var clienteEnderecoTelefoneDTO = ClienteEnderecoTelefoneDTO.builder()
                .record(Record.CLIENTE_ENDERECO_TELEFONE)
                .idCliente(pessoa.getId())
                .tipoEndereco(getTipoEndecoDto(pessoaEndereco))
                .nrSequencia(1)
                .tipoTelefone(ID_CLIENTE_TIPO_TELEFONE)
                .nrTelefone(pessoaEndereco.getTelefonePrincipal())
                .build();

        dto.addEnderecoTelefone(clienteEnderecoTelefoneDTO);
    }

    private void buildClienteIdsDto(Pessoa pessoa, PdvDadosClienteDTO dto) {

        for (TipoIdentificador tipoIdentificador : TipoIdentificador.values()) {

            buildClienteIdDto(pessoa, dto, tipoIdentificador);
        }
    }

    private void buildClienteIdDto(Pessoa pessoa, PdvDadosClienteDTO dto, TipoIdentificador tipoIdentificador) {

        var clienteIdDto = ClienteIdDTO.builder()
                .record(Record.CLIENTE_ID)
                .idCliente(pessoa.getId())
                .nrSequencia(tipoIdentificador.getCodigo())
                .tipoIdentificador(tipoIdentificador.getCodigo())
                .identificador(tipoIdentificador.getIdentificador(pessoa))
                .ativado(true)
                .build();

        dto.addId(clienteIdDto);
    }

    private void buildClienteRamoDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        var clienteRamoDto = ClienteRamoDTO.builder()
                .record(Record.CLIENTE_RAMO)
                .ramoAtividade(ID_RAMO_ATIVIDADE_PADRAO)
                .descricao(MessageUtil.get(I18NProperties.PADRAO))
                .build();

        pdvDadosClienteDTO.addRamo(clienteRamoDto);
    }

    private void buildClienteIdentificadorDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        for (TipoIdentificador tipoIdentificador: TipoIdentificador.values()) {

            buildClienteIdentificadorDto(pdvDadosClienteDTO, tipoIdentificador);
        }
    }

    private void buildClienteIdentificadorDto(
            PdvDadosClienteDTO pdvDadosClienteDTO,
            TipoIdentificador tipoIdentificador) {

        var clienteIdentificadorDto = ClienteIdentificadorDTO.builder()
                .record(Record.CLIENTE_IDENTIFICADOR)
                .tipoIdentificador(tipoIdentificador.getCodigo())
                .descricao(tipoIdentificador.getDescricao())
                .tamanho(tipoIdentificador.getTamanho())
                .tipoCampo(tipoIdentificador.getTipoCampo())
                .ativo(AtivoInativo.ATIVO)
                .enviaPdv(AtivoInativo.ATIVO)
                .build();

        pdvDadosClienteDTO.addIdentificador(clienteIdentificadorDto);
    }

    private void buildClienteConceitoDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        var clienteConceitoDto = ClienteConceitoDTO.builder()
                .record(Record.CLIENTE_CONCEITO)
                .id(ID_CLIENTE_CONCEITO)
                .descricao(MessageUtil.get(I18NProperties.PADRAO))
                .verificarDuplicataVencida(false)
                .nrDuplicacaVencida(0)
                .msgAlertaDuplicataVencida("")
                .vlDuplicadaVencida(BigDecimal.ZERO)
                .msgBloqueioVencimento("")
                .qtCarencia(0)
                .build();

        pdvDadosClienteDTO.addConceito(clienteConceitoDto);
    }

    private void buildClienteTipoTelefoneDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        var clienteTipoTelefoneDto = ClienteTipoTelefoneDTO.builder()
                .record(Record.CLIENTE_TIPO_TELEFONE)
                .tipoTelefone(ID_CLIENTE_TIPO_TELEFONE)
                .descricao(MessageUtil.get(I18NProperties.PADRAO))
                .build();

        pdvDadosClienteDTO.addTipoTelefone(clienteTipoTelefoneDto);
    }

    private void buildClienteTipoEnderecoDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        var clienteTipoEnderecoDto = ClienteTipoEnderecoDTO.builder()
                .record(Record.CLIENTE_TIPO_ENDERECO)
                .tipoEndereco(ID_CLIENTE_TIPO_ENDERECO)
                .descricao(MessageUtil.get(I18NProperties.PADRAO))
                .build();

        pdvDadosClienteDTO.addTipoEndereco(clienteTipoEnderecoDto);
    }

    private void buildClienteSituacaoCreditoDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        var clienteSituacaoCreditoDto = ClienteSituacaoCreditoDTO.builder()
                .record(Record.CLIENTE_SITUACAO_CREDITO)
                .tipoSituacao(ID_CLIENTE_SITUACAO_CREDITO)
                .descricao(MessageUtil.get(I18NProperties.PADRAO))
                .build();

        pdvDadosClienteDTO.addSituacaoCredito(clienteSituacaoCreditoDto);
    }

    private void buildClienteTipoDto(PdvDadosClienteDTO pdvDadosClienteDTO) {

        var clienteTipoDto = ClienteTipoDTO.builder()
                .record(Record.CLIENTE_TIPO)
                .tipoCliente(ID_CLIENTE_TIPO)
                .descricao(MessageUtil.get(I18NProperties.PADRAO))
                .idRamoAtividade(ID_RAMO_ATIVIDADE_PADRAO)
                .pcMaximoDesconto(BigDecimal.ZERO)
                .pcDescontoExtra(BigDecimal.ZERO)
                .emiteCupom(true)
                .idTipoPreco(ID_TIPO_PRECO)
                .idTipoPrecoExtra(ID_TIPO_PRECO_EXTRA)
                .build();

        pdvDadosClienteDTO.addTipo(clienteTipoDto);
    }

    private br.com.viasoft.integracaojetpdv.pdv.domain.cliente.TipoEndereco getTipoEndecoDto(PessoaEndereco pessoaEndereco) {
        if (TipoEndereco.PRINCIPAL.getTipo().equals(pessoaEndereco.getTipoEndereco())) {
            return br.com.viasoft.integracaojetpdv.pdv.domain.cliente.TipoEndereco.RESIDENCIAL;
        } else if (TipoEndereco.COBRANCA.getTipo().equals(pessoaEndereco.getTipoEndereco())) {
            return br.com.viasoft.integracaojetpdv.pdv.domain.cliente.TipoEndereco.COBRANCA;
        } else {
            return br.com.viasoft.integracaojetpdv.pdv.domain.cliente.TipoEndereco.COMERCIAL;
        }
    }

    private TipoCliente getTipoFisica(Pessoa entity) {

        return entity.getEhFisica() ? TipoCliente.FISICA : TipoCliente.JURIDICA;
    }

    private AtivoInativo getStatus(Pessoa entity) {

        return entity.getInativo() ? AtivoInativo.INATIVO : AtivoInativo.ATIVO;
    }

}
