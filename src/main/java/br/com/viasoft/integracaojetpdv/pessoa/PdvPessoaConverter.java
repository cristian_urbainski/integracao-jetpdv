package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.pdv.converter.IConverter;
import br.com.viasoft.integracaojetpdv.pdv.converter.PdvDadosDTOConverterData;
import br.com.viasoft.integracaojetpdv.pdv.domain.cliente.PdvDadosClienteDTO;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
public interface PdvPessoaConverter extends IConverter<PdvDadosDTOConverterData<Pessoa>, PdvDadosClienteDTO> {

}
