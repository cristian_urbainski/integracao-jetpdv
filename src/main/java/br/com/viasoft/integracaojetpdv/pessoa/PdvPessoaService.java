package br.com.viasoft.integracaojetpdv.pessoa;

import br.com.viasoft.integracaojetpdv.pdv.service.IPdvService;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
public interface PdvPessoaService extends IPdvService<Pessoa> {

}
