package br.com.viasoft.integracaojetpdv.pdv.converter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Cristian Urbainski
 * @since 14/04/2022
 */
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PdvDadosDTOConverterData<ENTITY> {

    private Long idFilial;
    private Integer numeroPagina;
    private Integer tamanhoPagina;
    private String uuid;
    private List<ENTITY> listEntities;

}
