package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.util.ListUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PdvDadosClienteDTO extends PdvDadosDTO {

    private List<ClienteRamoDTO> ramos;

    private List<ClienteTipoDTO> tipos;

    private List<ClienteSituacaoCreditoDTO> situacaoCreditos;

    private List<ClienteIdentificadorDTO> identificadores;

    private List<ClienteTipoEnderecoDTO> tipoEnderecos;

    private List<ClienteTipoTelefoneDTO> tipoTelefones;

    private List<ClienteConceitoDTO> conceitos;

    private List<ClienteDTO> clientes;

    private List<ClienteEnderecoDTO> enderecos;

    private List<ClienteEnderecoTelefoneDTO> enderecoTelefones;

    private List<ClienteIdDTO> ids;

    public void addRamo(ClienteRamoDTO dto) {

        this.ramos = ListUtil.emptyIfNull(this.ramos);
        this.ramos.add(dto);
    }

    public void addTipo(ClienteTipoDTO dto) {

        this.tipos = ListUtil.emptyIfNull(this.tipos);
        this.tipos.add(dto);
    }

    public void addSituacaoCredito(ClienteSituacaoCreditoDTO dto) {

        this.situacaoCreditos = ListUtil.emptyIfNull(this.situacaoCreditos);
        this.situacaoCreditos.add(dto);
    }

    public void addIdentificador(ClienteIdentificadorDTO dto) {

        this.identificadores = ListUtil.emptyIfNull(this.identificadores);
        this.identificadores.add(dto);
    }

    public void addTipoEndereco(ClienteTipoEnderecoDTO dto) {

        this.tipoEnderecos = ListUtil.emptyIfNull(this.tipoEnderecos);
        this.tipoEnderecos.add(dto);
    }

    public void addTipoTelefone(ClienteTipoTelefoneDTO dto) {

        this.tipoTelefones = ListUtil.emptyIfNull(this.tipoTelefones);
        this.tipoTelefones.add(dto);
    }

    public void addConceito(ClienteConceitoDTO dto) {

        this.conceitos = ListUtil.emptyIfNull(this.conceitos);
        this.conceitos.add(dto);
    }

    public void addCliente(ClienteDTO dto) {

        this.clientes = ListUtil.emptyIfNull(this.clientes);
        this.clientes.add(dto);
    }

    public void addEndereco(ClienteEnderecoDTO dto) {

        this.enderecos = ListUtil.emptyIfNull(this.enderecos);
        this.enderecos.add(dto);
    }

    public void addEnderecoTelefone(ClienteEnderecoTelefoneDTO dto) {

        this.enderecoTelefones = ListUtil.emptyIfNull(this.enderecoTelefones);
        this.enderecoTelefones.add(dto);
    }

    public void addId(ClienteIdDTO dto) {

        this.ids = ListUtil.emptyIfNull(this.ids);
        this.ids.add(dto);
    }

}
