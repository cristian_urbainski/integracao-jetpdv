package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
public enum TipoCliente {
    FISICA(1),
    JURIDICA(2);

    TipoCliente(int codigo) {

        this.codigo = codigo;
    }

    private final int codigo;

    @JsonValue
    public int getCodigo() {

        return codigo;
    }

}
