package br.com.viasoft.integracaojetpdv.pdv.controller;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface IPdvController {

    @RequestMapping("/all/{idFilial}/{pagina}/{quantidade}")
    ResponseEntity<PdvDadosDTO> findAll(
            @PathVariable("idFilial") Long idFilial,
            @PathVariable("pagina") Integer pagina,
            @PathVariable("quantidade") Integer quantidade);

    @RequestMapping("/all/{idFilial}/{pagina}")
    ResponseEntity<PdvDadosDTO> findAll(
            @PathVariable("idFilial") Long idFilial,
            @PathVariable("pagina") Integer pagina);

    @RequestMapping("/partial/{idFilial}/{pagina}/{quantidade}")
    ResponseEntity<PdvDadosDTO> findPartial(
            @PathVariable("idFilial") Long idFilial,
            @PathVariable("pagina") Integer pagina,
            @PathVariable("quantidade") Integer quantidade);

    @RequestMapping("/partial/{idFilial}/{pagina}")
    ResponseEntity<PdvDadosDTO> findPartial(
            @PathVariable("idFilial") Long idFilial,
            @PathVariable("pagina") Integer pagina);

}
