package br.com.viasoft.integracaojetpdv.pdv.domain.usuario;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.util.ListUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PdvDadosUsuarioDTO extends PdvDadosDTO {

    private List<UsuarioDTO> users;

    public void addUser(UsuarioDTO usuarioDTO) {

        this.users = ListUtil.emptyIfNull(users);
        this.users.add(usuarioDTO);
    }

}
