package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteRamoDTO extends PdvBaseDTO {
    @JsonProperty("RamoAtividade")
    private int ramoAtividade;
    @JsonProperty("Descricao")
    private String descricao;
}
