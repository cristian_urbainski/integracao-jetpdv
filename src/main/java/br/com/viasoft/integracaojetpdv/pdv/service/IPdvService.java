package br.com.viasoft.integracaojetpdv.pdv.service;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface IPdvService<ENTITY> {

    PdvDadosDTO findAll(Long idFilial, Integer pagina, Integer quantidade);

    PdvDadosDTO findAll(Long idFilial, Integer pagina);

    PdvDadosDTO findPartial(Long idFilial, Integer pagina, Integer quantidade);

    PdvDadosDTO findPartial(Long idFilial, Integer pagina);

}
