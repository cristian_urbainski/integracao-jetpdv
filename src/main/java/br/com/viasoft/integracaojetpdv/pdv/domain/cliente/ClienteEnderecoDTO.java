package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteEnderecoDTO extends PdvBaseDTO {
    @JsonProperty("Cliente")
    private Long idCliente;
    @JsonProperty("TipoEndereco")
    private TipoEndereco tipoEndereco;
    @JsonProperty("Cep")
    private String cep;
    @JsonProperty("CodigoMunicipio")
    private String codigoMunicipio;
    @JsonProperty("Uf")
    private String uf;
    @JsonProperty("Cidade")
    private String cidade;
    @JsonProperty("Bairro")
    private String bairro;
    @JsonProperty("Endereco")
    private String endereco;
    @JsonProperty("NumeroEndereco")
    private String nrEndereco;
    @JsonProperty("Complemento")
    private String complemento;
    private int idClienteTipoEndereco;
}
