package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.integracaojetpdv.pessoa.Pessoa;
import br.com.viasoft.rest.util.MessageUtil;
import br.com.viasoft.util.StringUtil;

/**
 * @author Cristian Urbainski
 * @since 26/04/2022
 */
public enum TipoIdentificador {
    ID(1) {
        @Override
        public String getIdentificador(Pessoa pessoa) {
            return StringUtil.toString(pessoa.getId());
        }

        @Override
        public String getDescricao() {
            return MessageUtil.get(I18NProperties.ID);
        }

        @Override
        public int getTamanho() {
            return 0;
        }

        @Override
        public TipoCampo getTipoCampo() {
            return TipoCampo.NUMERICO;
        }
    },
    NOME(2) {
        @Override
        public String getIdentificador(Pessoa pessoa) {
            return pessoa.getNome();
        }

        @Override
        public String getDescricao() {
            return MessageUtil.get(I18NProperties.NOME);
        }

        @Override
        public int getTamanho() {
            return 60;
        }

        @Override
        public TipoCampo getTipoCampo() {
            return TipoCampo.ALFANUMERICO;
        }
    },
    CPF_CNPJ(3) {
        @Override
        public String getIdentificador(Pessoa pessoa) {
            return pessoa.getIdentificacao();
        }

        @Override
        public String getDescricao() {
            return MessageUtil.get(I18NProperties.CPF_CNPJ);
        }

        @Override
        public int getTamanho() {
            return 14;
        }

        @Override
        public TipoCampo getTipoCampo() {
            return TipoCampo.NUMERICO;
        }
    };

    TipoIdentificador(int codigo) {

        this.codigo = codigo;
    }

    private final int codigo;

    public int getCodigo() {

        return codigo;
    }

    public abstract String getIdentificador(Pessoa pessoa);

    public abstract String getDescricao();

    public abstract int getTamanho();

    public abstract TipoCampo getTipoCampo();
}
