package br.com.viasoft.integracaojetpdv.pdv.domain.enumerator;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public enum Record {
    CLIENTE(20),
    CLIENTE_ENDERECO(21),
    CLIENTE_ENDERECO_TELEFONE(22),
    CLIENTE_ID(27),
    USUARIO(61),
    VENDEDOR(64),
    CLIENTE_TIPO(85),
    CLIENTE_RAMO(86),
    CLIENTE_SITUACAO_CREDITO(87),
    CLIENTE_IDENTIFICADOR(89),
    CLIENTE_TIPO_ENDERECO(90),
    CLIENTE_TIPO_TELEFONE(91),
    CLIENTE_CONCEITO(103);

    Record(int codigo) {

        this.codigo = codigo;
    }

    private final int codigo;

    @JsonValue
    public int getCodigo() {

        return codigo;
    }

}
