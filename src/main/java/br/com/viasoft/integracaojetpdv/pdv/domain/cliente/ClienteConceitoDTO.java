package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteConceitoDTO extends PdvBaseDTO {
    private int id;
    private String descricao;
    @JsonProperty("verifdupvenc")
    private boolean verificarDuplicataVencida;
    @JsonProperty("nrodupvenc")
    private int nrDuplicacaVencida;
    @JsonProperty("msgalertdupvenc")
    private String msgAlertaDuplicataVencida;
    @JsonProperty("vlrdupvenc")
    private BigDecimal vlDuplicadaVencida;
    @JsonProperty("msgbloqvenc")
    private String msgBloqueioVencimento;
    @JsonProperty("carencia")
    private int qtCarencia;
}
