package br.com.viasoft.integracaojetpdv.pdv.domain.usuario;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Getter
@SuperBuilder
public class UsuarioDTO extends PdvBaseDTO {

    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String password;
    @JsonProperty("status")
    private AtivoInativo status;
    @JsonProperty("vendedor")
    private Long idVendedor;

}
