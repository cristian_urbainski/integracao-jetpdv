package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteIdentificadorDTO extends PdvBaseDTO {
    @JsonProperty("TipoIdentificador")
    private int tipoIdentificador;
    @JsonProperty("Descricao")
    private String descricao;
    @JsonProperty("Tamanho")
    private int tamanho;
    @JsonProperty("TipoCampo")
    private TipoCampo tipoCampo;
    @JsonProperty("Ativo")
    private AtivoInativo ativo;
    @JsonProperty("EnviaParaPdv")
    private AtivoInativo enviaPdv;
}
