package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
public enum TipoCampo {
    ALFANUMERICO("A"),
    NUMERICO("N");

    TipoCampo(String codigo) {

        this.codigo = codigo;
    }

    private final String codigo;

    @JsonValue
    public String getCodigo() {
        return codigo;
    }

}
