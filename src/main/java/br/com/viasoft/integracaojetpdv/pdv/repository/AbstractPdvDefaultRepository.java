package br.com.viasoft.integracaojetpdv.pdv.repository;

import br.com.viasoft.integracaojetpdv.controlesincronizacao.ControleSincronizacao;
import br.com.viasoft.integracaojetpdv.i18n.I18NProperties;
import br.com.viasoft.jdbc.annotations.DataHoraUltimaAlteracaoAutomatico;
import br.com.viasoft.jdbc.exception.WarningException;
import br.com.viasoft.rest.util.MessageUtil;
import br.com.viasoft.util.ReflectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
@NoRepositoryBean
public abstract class AbstractPdvDefaultRepository<ENTITY> implements IPdvRepository<ENTITY> {

    protected Class<ENTITY> clazzPersistence;

    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    protected AbstractPdvDefaultRepository() {
        this.clazzPersistence = (Class<ENTITY>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public List<ENTITY> findAll(Long idFilial, Pageable pageable) {

        return find(idFilial, pageable, null);
    }

    @Override
    public List<ENTITY> findLastEntityUpdates(
            Long idFilial, Pageable pageable, ControleSincronizacao controleSincronizacao) {

        return find(idFilial, pageable, controleSincronizacao);
    }

    @Autowired
    public void setEntityManager(EntityManager entityManager) {

        this.entityManager = entityManager;
    }

    protected void applyPagination(TypedQuery<ENTITY> typedQuery, Pageable pageable) {

        if (typedQuery == null || pageable == null) {
            return;
        }

        typedQuery.setMaxResults(pageable.getPageSize());
        typedQuery.setFirstResult(Math.toIntExact(pageable.getOffset()));
    }

    protected void addJoins(Root<ENTITY> from) {

    }

    protected List<ENTITY> find(Long idFilial, Pageable pageable, ControleSincronizacao controleSincronizacao) {

        var cb = entityManager.getCriteriaBuilder();
        var cq = cb.createQuery(this.clazzPersistence);
        var from = cq.from(this.clazzPersistence);

        cq.distinct(true);

        addJoins(from);

        if (controleSincronizacao != null) {

            cq.where(mountDataHoraUltimaAlteracaoPredicate(cb, from, controleSincronizacao));
        }

        var typedQuery = entityManager.createQuery(cq);
        applyPagination(typedQuery, pageable);
        return typedQuery.getResultList();
    }

    protected Predicate mountDataHoraUltimaAlteracaoPredicate(
            CriteriaBuilder cb, Root<ENTITY> from, ControleSincronizacao controleSincronizacao) {

        var pathDataHoraUltimaAlteracao = from.get(getFieldAttributeDataHoraUltimaAlteracao());

        return cb.greaterThanOrEqualTo(pathDataHoraUltimaAlteracao, controleSincronizacao.getDataHoraSincronizacao());
    }

    @SuppressWarnings("unchecked")
    protected SingularAttribute<ENTITY, LocalDateTime> getFieldAttributeDataHoraUltimaAlteracao() {

        var field = ReflectionUtil.getFieldWithAnnotation(
                this.clazzPersistence, DataHoraUltimaAlteracaoAutomatico.class);

        if (field == null) {
            throw new WarningException(MessageUtil.get(
                    I18NProperties.CAMPO_DATA_HORA_ULTIMA_ALTERACAO_NAO_ENCONTRADO_NA_ENTIDADE,
                    this.clazzPersistence.getCanonicalName()));
        }

        var metamodel = entityManager.getMetamodel().entity(this.clazzPersistence);

        if (metamodel == null) {
            throw new WarningException(MessageUtil.get(
                    I18NProperties.NAO_FOI_ENCONTADO_METAMODELO_DA_CLASSE,
                    this.clazzPersistence.getCanonicalName()));
        }

        Attribute<?, ?> attribute;
        try {

            attribute = metamodel.getAttribute(field.getName());
        } catch (IllegalArgumentException ex) {

            throw new WarningException(MessageUtil.get(
                    I18NProperties.CAMPO_DATA_HORA_ULTIMA_ALTERACAO_NAO_ENCONTRADO_NO_METAMODELO,
                    this.clazzPersistence.getCanonicalName()), ex);
        }

        if (attribute instanceof SingularAttribute && LocalDateTime.class.equals(attribute.getJavaType())) {

            return (SingularAttribute<ENTITY, LocalDateTime>) attribute;
        }

        throw new WarningException(MessageUtil.get(
                I18NProperties.CAMPO_DATA_HORA_ULTIMA_ALTERACAO_DE_FORMA_DIFERENTE_DO_ESPERADO_NO_METAMODELO,
                this.clazzPersistence.getCanonicalName()));
    }

}
