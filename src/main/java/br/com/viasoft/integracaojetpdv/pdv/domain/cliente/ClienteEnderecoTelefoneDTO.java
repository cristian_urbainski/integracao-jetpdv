package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteEnderecoTelefoneDTO extends PdvBaseDTO {
    @JsonProperty("Cliente")
    private Long idCliente;
    @JsonProperty("TipoEndereco")
    private TipoEndereco tipoEndereco;
    @JsonProperty("Sequencia")
    private int nrSequencia;
    @JsonProperty("TipoTelefone")
    private int tipoTelefone;
    @JsonProperty("NumeroTelefone")
    private String nrTelefone;
}
