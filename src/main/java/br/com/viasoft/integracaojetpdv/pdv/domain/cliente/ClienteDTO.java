package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
@Getter
@SuperBuilder
public class ClienteDTO extends PdvBaseDTO {

    @JsonProperty("Codigo")
    private Long id;
    @JsonProperty("Nome")
    private String nome;
    @JsonProperty("Situacao")
    private AtivoInativo status;
    @JsonProperty("TipoCliente")
    private TipoCliente tipoCliente;
    @JsonProperty("CPF_CNPJ")
    private String cpfCnpj;
    @JsonProperty("RG_IE")
    private String rgIe;
    @JsonProperty("UF_RG")
    private String ufRg;
    @JsonProperty("Limite")
    private BigDecimal valorLimiteCredito;
    @JsonProperty("Utilizado")
    private BigDecimal valorUtilizadoLimiteCredito;
    @JsonProperty("DataCadastro")
    private LocalDate dataCadastro;
    @JsonProperty("DataVencimento")
    private LocalDate dataRevisaoLimiteCredito;
    @JsonProperty("ie")
    private String nrInscricaoEstadual;
    private String email;
    private int idClienteTipo;
    private int idClienteSituacaoCredito;
    private int idClienteConceito;
}
