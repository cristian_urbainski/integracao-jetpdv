package br.com.viasoft.integracaojetpdv.pdv.converter;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public interface IConverter<OF, TO> {

    TO convert(OF entity);

}
