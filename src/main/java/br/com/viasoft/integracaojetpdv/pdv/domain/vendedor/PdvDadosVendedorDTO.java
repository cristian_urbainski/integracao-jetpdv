package br.com.viasoft.integracaojetpdv.pdv.domain.vendedor;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.util.ListUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PdvDadosVendedorDTO extends PdvDadosDTO {

    private List<VendedorDTO> vendedores;

    public void addVendedor(VendedorDTO vendedorDTO) {

        this.vendedores = ListUtil.emptyIfNull(vendedores);
        this.vendedores.add(vendedorDTO);
    }

}
