package br.com.viasoft.integracaojetpdv.pdv.domain.commons;

import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.Record;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@SuperBuilder
public abstract class PdvBaseDTO {

    @JsonProperty("Record")
    protected Record record;

}
