package br.com.viasoft.integracaojetpdv.pdv.controller;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.integracaojetpdv.pdv.service.IPdvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public abstract class AbstractPdvDefaultController<ENTITY> implements IPdvController {

    private IPdvService<ENTITY> service;

    @Override
    public ResponseEntity<PdvDadosDTO> findAll(Long idFilial, Integer pagina, Integer quantidade) {

        var dados = service.findAll(idFilial, pagina, quantidade);
        return ResponseEntity.ok(dados);
    }

    @Override
    public ResponseEntity<PdvDadosDTO> findAll(Long idFilial, Integer pagina) {

        var dados = service.findAll(idFilial, pagina);
        return ResponseEntity.ok(dados);
    }

    @Override
    public ResponseEntity<PdvDadosDTO> findPartial(Long idFilial, Integer pagina, Integer quantidade) {

        var dados = service.findPartial(idFilial, pagina, quantidade);
        return ResponseEntity.ok(dados);
    }

    @Override
    public ResponseEntity<PdvDadosDTO> findPartial(Long idFilial, Integer pagina) {

        var dados = service.findPartial(idFilial, pagina);
        return ResponseEntity.ok(dados);
    }

    @Autowired
    public void setService(IPdvService<ENTITY> service) {
        this.service = service;
    }

}
