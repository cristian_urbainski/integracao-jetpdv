package br.com.viasoft.integracaojetpdv.pdv.domain.vendedor;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import br.com.viasoft.integracaojetpdv.pdv.domain.enumerator.AtivoInativo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

/**
 * @author Cristian Urbainski
 * @since 23/04/2022
 */
@Getter
@SuperBuilder
public class VendedorDTO extends PdvBaseDTO {

    @JsonProperty("Vendedor")
    private Long idVendedor;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Nrocartao")
    private String nrCartao;
    @JsonProperty("Ativo")
    private AtivoInativo status;
    @JsonProperty("Comissao")
    private BigDecimal valorComissao;

}
