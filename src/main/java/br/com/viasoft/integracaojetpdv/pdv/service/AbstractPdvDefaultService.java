package br.com.viasoft.integracaojetpdv.pdv.service;

import br.com.viasoft.integracaojetpdv.controlesincronizacao.ControleSincronizacaService;
import br.com.viasoft.integracaojetpdv.pdv.converter.AbstractPdvDadosDTOConverter;
import br.com.viasoft.integracaojetpdv.pdv.converter.PdvDadosDTOConverterData;
import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.integracaojetpdv.pdv.repository.IPdvRepository;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import br.com.viasoft.integracaojetpdv.sincronizacaolog.SincronizacaoLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.UUID;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public abstract class AbstractPdvDefaultService<ENTITY, DTO extends PdvDadosDTO> implements IPdvService<ENTITY> {

    private static final int DEFAULT_QUANTIDADE = 100;

    private IPdvRepository<ENTITY> repository;

    private AbstractPdvDadosDTOConverter<ENTITY, DTO> converter;

    private ControleSincronizacaService controleSincronizacaService;

    private SincronizacaoLogService sincronizacaoLogService;

    protected PdvDadosDTO findAll(Long idFilial, Integer pagina, Integer quantidade, boolean isConverterCabecalhos) {

        var uuid = UUID.randomUUID().toString();

        var list = repository.findAll(idFilial, PageRequest.of(pagina, quantidade));

        var data = buildPdvDadosDTOConverterData(idFilial, pagina, quantidade, uuid, list);

        var dados = converter.convert(data, isConverterCabecalhos);

        if (noDataReturned(dados)) {

            var countLogs = sincronizacaoLogService.countBy(data.getUuid());

            if (countLogs > 0) {

                return findAll(idFilial, ++pagina, quantidade, isConverterCabecalhos);
            } else {

                controleSincronizacaService.createOrUpdate(
                        idFilial, getRotinaSincronizacao(), TipoIntegracao.JETPDV);
            }
        }

        return dados;
    }

    @Override
    public PdvDadosDTO findAll(Long idFilial, Integer pagina, Integer quantidade) {

        return findAll(idFilial, pagina, quantidade, isConverterCabecalhos(pagina));
    }

    @Override
    public PdvDadosDTO findAll(Long idFilial, Integer pagina) {

        return findAll(idFilial, pagina, DEFAULT_QUANTIDADE);
    }

    public PdvDadosDTO findPartial(Long idFilial, Integer pagina, Integer quantidade, boolean isConverterCabecalhos) {

        var optionalControleSincronizacao = controleSincronizacaService.findBy(
                idFilial, getRotinaSincronizacao(), TipoIntegracao.JETPDV);

        if (optionalControleSincronizacao.isEmpty()) {

            return findAll(idFilial, pagina, quantidade);
        }

        var uuid = UUID.randomUUID().toString();

        var list = repository.findLastEntityUpdates(
                idFilial, PageRequest.of(pagina, quantidade), optionalControleSincronizacao.get());

        var data = buildPdvDadosDTOConverterData(idFilial, pagina, quantidade, uuid, list);

        var dados = converter.convert(data, isConverterCabecalhos);

        if (noDataReturned(dados)) {

            var countLogs = sincronizacaoLogService.countBy(uuid);

            if (countLogs > 0) {

                return findPartial(idFilial, ++pagina, quantidade, isConverterCabecalhos);
            } else {

                controleSincronizacaService.createOrUpdate(
                        idFilial, getRotinaSincronizacao(), TipoIntegracao.JETPDV);
            }
        }

        return dados;
    }

    @Override
    public PdvDadosDTO findPartial(Long idFilial, Integer pagina, Integer quantidade) {

        return findPartial(idFilial, pagina, quantidade, isConverterCabecalhos(pagina));
    }

    @Override
    public PdvDadosDTO findPartial(Long idFilial, Integer pagina) {

        return findPartial(idFilial, pagina, DEFAULT_QUANTIDADE);
    }

    @Autowired
    public void setRepository(IPdvRepository<ENTITY> repository) {

        this.repository = repository;
    }

    @Autowired
    public void setConverter(AbstractPdvDadosDTOConverter<ENTITY, DTO> converter) {

        this.converter = converter;
    }

    @Autowired
    public void setSincronizacaoLogService(SincronizacaoLogService sincronizacaoLogService) {

        this.sincronizacaoLogService = sincronizacaoLogService;
    }

    @Autowired
    public void setControleSincronizacaService(ControleSincronizacaService controleSincronizacaService) {

        this.controleSincronizacaService = controleSincronizacaService;
    }

    private PdvDadosDTOConverterData<ENTITY> buildPdvDadosDTOConverterData(
            Long idFilial,
            Integer pagina,
            Integer quantidade,
            String uuid,
            List<ENTITY> list) {

        return PdvDadosDTOConverterData.<ENTITY>builder()
                .idFilial(idFilial)
                .numeroPagina(pagina)
                .tamanhoPagina(quantidade)
                .uuid(uuid)
                .listEntities(list)
                .build();
    }

    protected abstract RotinaSincronizacao getRotinaSincronizacao();

    protected abstract boolean noDataReturned(DTO dto);

    protected boolean isConverterCabecalhos(int pagina) {

        return pagina == 0;
    }

}
