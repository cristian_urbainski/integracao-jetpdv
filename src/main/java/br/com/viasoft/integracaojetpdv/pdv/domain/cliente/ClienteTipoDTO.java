package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteTipoDTO extends PdvBaseDTO {
    @JsonProperty("TipoCliente")
    private int tipoCliente;
    @JsonProperty("Descricao")
    private String descricao;
    @JsonProperty("RamoAtividade")
    private int idRamoAtividade;
    @JsonProperty("MaximoDesconto")
    private BigDecimal pcMaximoDesconto;
    @JsonProperty("DescontoExtra")
    private BigDecimal pcDescontoExtra;
    @JsonProperty("EmiteCupom")
    private boolean emiteCupom;
    @JsonProperty("TipoPreco")
    private int idTipoPreco;
    @JsonProperty("TipoPrecoExtra")
    private int idTipoPrecoExtra;
}
