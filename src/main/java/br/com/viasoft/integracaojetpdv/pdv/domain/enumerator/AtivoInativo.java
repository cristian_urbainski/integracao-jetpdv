package br.com.viasoft.integracaojetpdv.pdv.domain.enumerator;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
public enum AtivoInativo {
    ATIVO(1),
    INATIVO(0);

    AtivoInativo(int codigo) {

        this.codigo = codigo;
    }

    private final int codigo;

    @JsonValue
    public int getCodigo() {

        return codigo;
    }

}
