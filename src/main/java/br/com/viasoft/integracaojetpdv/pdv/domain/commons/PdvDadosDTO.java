package br.com.viasoft.integracaojetpdv.pdv.domain.commons;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 12/04/2022
 */
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class PdvDadosDTO {

    @Setter
    private Integer pagina;

    @Setter
    private Integer quantidade;

}
