package br.com.viasoft.integracaojetpdv.pdv.converter;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvDadosDTO;
import br.com.viasoft.integracaojetpdv.sincronizacao.RotinaSincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.Sincronizacao;
import br.com.viasoft.integracaojetpdv.sincronizacao.SincronizacaoService;
import br.com.viasoft.integracaojetpdv.sincronizacao.TipoIntegracao;
import br.com.viasoft.integracaojetpdv.sincronizacaolog.SincronizacaoLogService;
import br.com.viasoft.jdbc.exception.WarningException;
import br.com.viasoft.util.ListUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

/**
 * @author Cristian Urbainski
 * @since 14/04/2022
 */
public abstract class AbstractPdvDadosDTOConverter<OF, TO extends PdvDadosDTO>
        implements IConverter<PdvDadosDTOConverterData<OF>, TO> {

    protected SincronizacaoService sincronizacaoService;
    protected SincronizacaoLogService sincronizacaoLogService;
    private final Class<TO> classTo;

    @SuppressWarnings("unchecked")
    protected AbstractPdvDadosDTOConverter() {
        this.classTo = (Class<TO>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Override
    public TO convert(PdvDadosDTOConverterData<OF> data) {

        var to = initToObject(data);

        if (ListUtil.isEmpty(data.getListEntities())) {

            return to;
        }

        data.getListEntities().forEach(entity -> {

            var isValidEntity = isValidEntity(entity);

            if (isValidEntity) {

                convertEntity(entity, to);
            } else {

                var sincronizacao = sincronizacaoService.findOrCreate(
                        data.getUuid(),
                        data.getIdFilial(),
                        data.getNumeroPagina(),
                        data.getTamanhoPagina(),
                        getRotinaSincronizacao(),
                        TipoIntegracao.JETPDV);

                saveSincronizacaoLogs(sincronizacao, entity);
            }
        });

        return to;
    }

    public TO convert(PdvDadosDTOConverterData<OF> data, boolean isConverterCabecalhos) {

        var to = convert(data);

        if (isConverterCabecalhos) {

            converterCabecalhos(to);
        }

        return to;
    }

    protected void converterCabecalhos(TO to) {

    }

    protected abstract boolean isValidEntity(OF entity);

    protected abstract void saveSincronizacaoLogs(Sincronizacao sincronizacao, OF entity);

    protected abstract void convertEntity(OF entity, TO to);

    protected abstract RotinaSincronizacao getRotinaSincronizacao();

    protected TO initToObject(PdvDadosDTOConverterData<OF> data) {

        TO dados;
        try {
            dados = this.classTo.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new WarningException(e.getMessage(), e);
        }

        dados.setPagina(data.getNumeroPagina());
        dados.setQuantidade(data.getTamanhoPagina());

        return dados;
    }

    @Autowired
    public void setSincronizacaoLogService(SincronizacaoLogService sincronizacaoLogService) {
        this.sincronizacaoLogService = sincronizacaoLogService;
    }

    @Autowired
    public void setSincronizacaoService(SincronizacaoService sincronizacaoService) {
        this.sincronizacaoService = sincronizacaoService;
    }

}
