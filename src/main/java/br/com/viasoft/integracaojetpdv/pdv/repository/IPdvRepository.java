package br.com.viasoft.integracaojetpdv.pdv.repository;

import br.com.viasoft.integracaojetpdv.controlesincronizacao.ControleSincronizacao;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Cristian Urbainski
 * @since 13/04/2022
 */
public interface IPdvRepository<ENTITY> {

    List<ENTITY> findAll(Long idFilial, Pageable pageable);

    List<ENTITY> findLastEntityUpdates(Long idFilial, Pageable pageable, ControleSincronizacao controleSincronizacao);

}
