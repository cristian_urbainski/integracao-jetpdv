package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
public enum TipoEndereco {
    RESIDENCIAL(1),
    COMERCIAL(2),
    ENTREGA(3),
    COBRANCA(4);

    TipoEndereco(int codigo) {

        this.codigo = codigo;
    }

    private final int codigo;

    @JsonValue
    public int getCodigo() {

        return codigo;
    }

}
