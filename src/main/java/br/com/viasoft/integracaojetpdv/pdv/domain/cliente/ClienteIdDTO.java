package br.com.viasoft.integracaojetpdv.pdv.domain.cliente;

import br.com.viasoft.integracaojetpdv.pdv.domain.commons.PdvBaseDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Cristian Urbainski
 * @since 25/04/2022
 */
@Getter
@SuperBuilder
public class ClienteIdDTO extends PdvBaseDTO {
    @JsonProperty("Cliente")
    private Long idCliente;
    @JsonProperty("Sequencia")
    private int nrSequencia;
    @JsonProperty("TipoIdentificador")
    private int tipoIdentificador;
    @JsonProperty("Identificador")
    private String identificador;
    @JsonProperty("Ativado")
    private boolean ativado;
}
