package br.com.viasoft.integracaojetpdv.timeZone;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "TIME_ZONE", uniqueConstraints = {@UniqueConstraint(name = "TIME_ZONE_UNIQUE",
        columnNames = {"TIME_ZONE"})}, catalog = "VIASOFT")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TimeZone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TIME_ZONE")
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Column(name = "TIME_ZONE", length = 50, nullable = false, unique = true)
    private String timeZone;

    @NotBlank
    @Size(max = 10)
    @Column(name = "UTC_OFFSET", length = 10, nullable = false)
    private String utcOffset;

    @NotBlank
    @Size(max = 10)
    @Column(name = "UTC_DST_OFFSET", length = 10, nullable = false)
    private String utcDstOffset;


}
