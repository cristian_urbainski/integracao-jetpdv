package br.com.viasoft.architecture.database;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.runner.RunWith;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.sql.SQLException;
import java.util.LinkedHashSet;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class SpringDataTest extends DatabaseTest {

    @ArchTest
    public void sqlSintax(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valida sintax sql das queries nativas") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                item.getMethods().forEach(method -> {
                    if (method.isAnnotatedWith(Query.class) && !method.isAnnotatedWith(Modifying.class)) {
                        var query = method.getAnnotationOfType(Query.class);
                        if (query.nativeQuery()) {
                            var sql = query.value().replace("?", "''");
                            var parametros = new LinkedHashSet<>(getParametersSql(sql));
                            for (String parametro : parametros) {
                                sql = replaceSqlParameters(sql, parametro);
                            }
                            try {
                                statement.executeQuery(sql);
                            } catch (SQLException e) {
                                events.add(SimpleConditionEvent.violated(item, "Erro de sintax sql no método " + method.getName() + " da interface " + item.getSimpleName()));
                            }
                        }
                    }
                });
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areInterfaces()
                .and().areAssignableTo(JpaRepository.class)
                .should(condition);

        rule.check(classes);
    }

    private String replaceSqlParameters(String sql, String parametro) {
        sql = sql.replace(":" + parametro, "null");
        sql = sql.replaceAll(":#\\{#" + parametro + "(.*?)}", "null");
        sql = sql.replaceAll("(?i)limit null", "LIMIT 1");
        return sql;
    }

}
