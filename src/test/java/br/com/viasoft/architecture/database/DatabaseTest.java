package br.com.viasoft.architecture.database;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Log4j2
public abstract class DatabaseTest {

    public static final String DATABASE_SCHEMA_TEST = "CLIENTE_2374";
    public static Statement statement;
    private static Connection connection;

    @BeforeClass
    public static void init() {
        try {
            connection = DatabaseTest.getConnection();
            statement = connection.createStatement();
            statement.executeQuery(String.format("use %s;", DATABASE_SCHEMA_TEST));
        } catch (SQLException se) {
            log.log(Level.ERROR, "Não foi possivel conectar no banco de dados");
            throw new RuntimeException(se);
        }
    }

    @AfterClass
    public static void finish() {
        try {
            statement.close();
        } catch (SQLException e) {
            log.log(Level.TRACE, "Não foi possivel fechar o statement");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            log.log(Level.TRACE, "Não foi possivel fechar a conection");
        }
    }

    private static Connection getConnection() {
        try {
            return DriverManager.getConnection(DatabaseTest.getUrl(), DatabaseTest.getUsername(), DatabaseTest.getPassword());
        } catch (SQLException se) {
            log.log(Level.ERROR, "Não foi possivel conectar no banco de dados");
            throw new RuntimeException(se);
        }
    }

    private static String getUrl() {
        var databaseIp = Optional.ofNullable(System.getProperty("databaseIp")).orElse("192.168.3.146");
        return String.format("jdbc:mysql://%s/?useSSL=false", databaseIp);
    }

    private static String getUsername() {
        return Optional.ofNullable(System.getProperty("databaseUsername")).orElse("viasoft");
    }

    private static String getPassword() {
        return Optional.ofNullable(System.getProperty("databaseUsername")).orElse("V1@s0ftN1m17z");
    }

    protected List<String> getParametersSql(String sql) {
        List<String> parameterList = new LinkedList<>();
        Matcher matcher = Pattern.compile("\\:([^\\s\\'.?!:=]+)").matcher(sql);
        while (matcher.find()) {
            if (matcher.group().equalsIgnoreCase(":SCHEMA"))
                continue;

            parameterList.add(matcher.group().substring(1).replaceAll("[^_a-zA-Z0-9]+", ""));
        }
        return parameterList.stream()
                .sorted(Comparator.comparing(String::length, Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

}
