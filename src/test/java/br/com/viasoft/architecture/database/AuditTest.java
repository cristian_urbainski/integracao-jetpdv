package br.com.viasoft.architecture.database;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.junit.runner.RunWith;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class AuditTest extends DatabaseTest {

    @ArchTest
    public void existColumnInTableOfAudit(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Coluna não existe na tabela de auditoria") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var columnList = getColumnListByTable(table.name());
                item.getAllFields().forEach(field -> {
                    if (field.isAnnotatedWith(Column.class) && !field.isAnnotatedWith(NotAudited.class)) {
                        var column = field.getAnnotationOfType(Column.class);
                        if (!columnList.contains(column.name())) {
                            var message = String.format("Coluna %s não existe na tabela %s_AUD utilizada para auditoria", column.name(), table.name());
                            events.add(SimpleConditionEvent.violated(field, message));
                        }
                    }
                });
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .and().areAnnotatedWith(Table.class)
                .and().areAnnotatedWith(Audited.class)
                .should(condition);

        rule.check(classes);
    }

    private List<String> getColumnListByTable(String table) {
        String query = String.format("SELECT COLUMN_NAME FROM information_schema.COLUMNS " +
                "WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s_AUD'", DATABASE_SCHEMA_TEST, table);
        try (ResultSet resultSet = statement.executeQuery(query)) {
            List<String> columnNameList = new ArrayList<>();
            while (resultSet.next()) {
                columnNameList.add(resultSet.getString("COLUMN_NAME"));
            }
            return columnNameList;
        } catch (SQLException se) {
            throw new RuntimeException(se);
        }
    }

}
