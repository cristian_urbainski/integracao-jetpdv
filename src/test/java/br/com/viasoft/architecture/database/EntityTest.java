package br.com.viasoft.architecture.database;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaField;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import lombok.AllArgsConstructor;
import org.junit.runner.RunWith;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class EntityTest extends DatabaseTest {

    @ArchTest
    public static final ArchRule isTable = ArchRuleDefinition.classes()
            .that().arePublic()
            .and().areAnnotatedWith(Entity.class)
            .should().beAnnotatedWith(Table.class);
    private static Map<String, List<EntityInfo>> mapEntityInfoList = new HashMap<>();

    @ArchTest
    public void precision(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valor da propriedade precision com o banco de dados") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var databaseInfoList = getColumnListByTable(table);
                for (JavaField field : item.getAllFields()) {
                    if (field.isAnnotatedWith(Column.class) && field.getRawType().isAssignableFrom(BigDecimal.class)) {
                        var column = field.getAnnotationOfType(Column.class);
                        databaseInfoList.stream()
                                .filter(c -> c.column.equals(column.name()))
                                .forEach(d -> {
                                    if (d.precision != column.precision()) {
                                        var message = String.format(
                                                "Precision da classe.propriedade %s.%s que esta no pacote %s está com valor %d mas no banco é %d",
                                                item.getSimpleName(),
                                                field.getName(),
                                                item.getPackageName(),
                                                column.precision(),
                                                d.precision);
                                        events.add(SimpleConditionEvent.violated(field, message));
                                    }
                                });
                    }
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .and().areAnnotatedWith(Table.class)
                .should(condition);

        rule.check(classes);
    }

    @ArchTest
    public void scale(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valor da propriedade scale com o banco de dados") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var databaseInfoList = getColumnListByTable(table);
                for (JavaField field : item.getAllFields()) {
                    if (field.isAnnotatedWith(Column.class) && field.getRawType().isAssignableFrom(BigDecimal.class)) {
                        var column = field.getAnnotationOfType(Column.class);
                        databaseInfoList.stream()
                                .filter(c -> c.column.equals(column.name()))
                                .forEach(d -> {
                                    if (d.scale != column.scale()) {
                                        var message = String.format(
                                                "Scale da classe.propriedade %s.%s que esta no pacote %s está com valor %d mas no banco é %d",
                                                item.getSimpleName(),
                                                field.getName(),
                                                item.getPackageName(),
                                                column.scale(),
                                                d.scale);
                                        events.add(SimpleConditionEvent.violated(field, message));
                                    }
                                });
                    }
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .should(condition);

        rule.check(classes);
    }

    // Teste ignorado, pois, entendemos que nem sempre um campo que é notnull na classe será notnull no banco
    @ArchIgnore
    @ArchTest
    public void nullable(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valor da propriedade nullable com o banco de dados") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var databaseInfoList = getColumnListByTable(table);
                for (JavaField field : item.getAllFields()) {
                    if (field.isAnnotatedWith(Column.class) && !field.isAnnotatedWith(Id.class)) {
                        var column = field.getAnnotationOfType(Column.class);
                        databaseInfoList.stream()
                                .filter(c -> c.column.equals(column.name()))
                                .filter(d -> !d.isNullable && column.nullable())
                                .forEach(d -> {
                                    var message = String.format(
                                            "Nullable da propriedade %s da classe %s que esta no pacote %s está com valor %b mas no banco é %b",
                                            field.getName(),
                                            item.getSimpleName(),
                                            item.getPackageName(),
                                            column.nullable(),
                                            d.isNullable);
                                    events.add(SimpleConditionEvent.violated(field, message));
                                });
                    }
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .should(condition);

        rule.check(classes);
    }

    @ArchTest
    public void length(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valor da propriedade length com o banco de dados") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var databaseInfoList = getColumnListByTable(table);
                for (JavaField field : item.getAllFields()) {
                    if (field.isAnnotatedWith(Column.class) && field.getRawType().isAssignableFrom(String.class)) {
                        var column = field.getAnnotationOfType(Column.class);
                        databaseInfoList.stream()
                                .filter(c -> c.column.equals(column.name()))
                                .filter(d -> "varchar".equals(d.type) || "enum".equals(d.type))
                                .forEach(d -> {
                                    if (d.maxLength != column.length()) {
                                        var message = String.format(
                                                "Length da classe.propriedade %s.%s que esta no pacote %s está com valor %d mas no banco é %d",
                                                item.getSimpleName(),
                                                field.getName(),
                                                item.getPackageName(),
                                                column.length(),
                                                d.maxLength);
                                        events.add(SimpleConditionEvent.violated(field, message));
                                    }
                                });
                    }
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .should(condition);

        rule.check(classes);
    }

    @ArchTest
    public void lengthEqualsSize(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valor da propriedade length da anotação " +
                "column é diferente do valor da propriedade max da anotação size") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                for (JavaField field : item.getAllFields()) {
                    if (field.isAnnotatedWith(Column.class)
                            && field.isAnnotatedWith(Size.class)
                            && field.getRawType().isAssignableFrom(String.class)) {
                        var column = field.getAnnotationOfType(Column.class);
                        var size = field.getAnnotationOfType(Size.class);
                        if (column.length() != size.max()) {
                            var message = String.format(
                                    "Length da propriedade %s da classe %s que esta no pacote %s está com valor %d mas no size da anotação size é %d",
                                    field.getName(),
                                    item.getSimpleName(),
                                    item.getPackageName(),
                                    column.length(),
                                    size.max());
                            events.add(SimpleConditionEvent.violated(field, message));
                        }
                    }
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .should(condition);

        rule.check(classes);
    }

    @ArchTest
    public void tableExistInDatabase(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valida se entidade existe no banco de dados") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var databaseInfoList = getColumnListByTable(table);

                if (databaseInfoList == null || databaseInfoList.isEmpty()) {
                    var message = String.format(
                            "Entidade %s da classe %s que esta no pacote %s não consta no banco de dados",
                            table.name(),
                            item.getSimpleName(),
                            item.getPackageName());
                    events.add(SimpleConditionEvent.violated(table, message));
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .should(condition);

        rule.check(classes);
    }

    @ArchTest
    public void tableColumnExistInDatabase(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("Valida se campos existem no banco de dados") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var table = item.getAnnotationOfType(Table.class);
                var databaseInfoList = getColumnListByTable(table);
                var databaseInfoColumnMap = databaseInfoList.stream().collect(Collectors.toMap(info -> info.column, info -> info));

                var fieldNames = item.getAllFields().stream()
                        .map(field -> {
                            if (field.isAnnotatedWith(Column.class)) {
                                return field.getAnnotationOfType(Column.class).name();
                            } else if (field.isAnnotatedWith(JoinColumn.class)) {
                                return field.getAnnotationOfType(JoinColumn.class).name();
                            }
                            return null;
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                for (String fieldName : fieldNames) {
                    if (!databaseInfoColumnMap.containsKey(fieldName)) {
                        var message = String.format(
                                "A coluna %s da entidade %s da classe %s que esta no pacote %s não consta no banco de dados",
                                fieldName,
                                table.name(),
                                item.getSimpleName(),
                                item.getPackageName());
                        events.add(SimpleConditionEvent.violated(table, message));
                    }
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areAnnotatedWith(Entity.class)
                .should(condition);

        rule.check(classes);
    }

    private synchronized List<EntityInfo> getColumnListByTable(Table table) {
        if (mapEntityInfoList.get(table.name()) == null) {
            final String catalog = table.catalog().isEmpty() ? DATABASE_SCHEMA_TEST : table.catalog();

            String query = String.format("SELECT " +
                    "COLUMN_NAME, " +
                    "COALESCE(NUMERIC_PRECISION, 0) AS NUMERIC_PRECISION, " +
                    "COALESCE(NUMERIC_SCALE, 0) AS NUMERIC_SCALE, " +
                    "IS_NULLABLE, " +
                    "COALESCE(CHARACTER_MAXIMUM_LENGTH, 0) AS MAX_LENGTH, " +
                    "DATA_TYPE " +
                    "FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s'", catalog, table.name());

            try (ResultSet resultSet = statement.executeQuery(query)) {
                List<EntityInfo> entityInfoList = new ArrayList<>();
                while (resultSet.next()) {
                    var column = resultSet.getString("COLUMN_NAME");
                    var precision = resultSet.getInt("NUMERIC_PRECISION");
                    var scale = resultSet.getInt("NUMERIC_SCALE");
                    var isNullable = "YES".equalsIgnoreCase(resultSet.getString("IS_NULLABLE").trim());
                    var maxLength = resultSet.getInt("MAX_LENGTH");
                    var type = resultSet.getString("DATA_TYPE");
                    entityInfoList.add(new EntityInfo(column, precision, scale, isNullable, maxLength, type));
                }
                mapEntityInfoList.put(table.name(), entityInfoList);
                return entityInfoList;
            } catch (SQLException se) {
                throw new RuntimeException(se);
            }
        }
        return mapEntityInfoList.get(table.name());
    }

    @AllArgsConstructor
    private class EntityInfo {

        private String column;
        private int precision;
        private int scale;
        private boolean isNullable;
        private int maxLength;
        private String type;

    }

}
