package br.com.viasoft.architecture.pdv;

import br.com.viasoft.integracaojetpdv.pdv.controller.IPdvController;
import br.com.viasoft.security.annotation.Protected;
import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Cristian Urbainski
 * @since 19/04/2022
 */
@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class PdvControllerTest {

    @ArchTest
    public static final ArchRule controllerImpl = ArchRuleDefinition.classes()
            .that().arePublic()
            .and().haveSimpleNameStartingWith("Pdv")
            .and().haveSimpleNameEndingWith("ControllerImpl")
            .and().doNotHaveModifier(JavaModifier.ABSTRACT)
            .should().beAssignableTo(IPdvController.class)
            .andShould().beAnnotatedWith(Protected.class)
            .andShould().beAnnotatedWith(Controller.class)
            .orShould().beAnnotatedWith(RestController.class);

}
