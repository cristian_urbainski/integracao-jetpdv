package br.com.viasoft.architecture;

import br.com.viasoft.security.annotation.Protected;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.apache.commons.lang3.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class FeignClientTest {

    @ArchTest
    public static final ArchRule feignClientItCanNotBeController =
            ArchRuleDefinition.classes()
                    .that().arePublic()
                    .and().areInterfaces()
                    .and().haveSimpleNameEndingWith("Controller")
                    .and().resideOutsideOfPackages(
                            "br.com.viasoft.rest.controller", "br.com.viasoft.integracaojetpdv.pdv.controller")
                    .should().notBeAnnotatedWith(Controller.class)
                    .andShould().beAnnotatedWith(FeignClient.class);

    @ArchTest
    public static final ArchRule feignClientItCanNotBeRestController =
            ArchRuleDefinition.classes()
                    .that().arePublic()
                    .and().areInterfaces()
                    .and().haveSimpleNameEndingWith("Controller")
                    .and().resideOutsideOfPackages(
                            "br.com.viasoft.rest.controller", "br.com.viasoft.integracaojetpdv.pdv.controller")
                    .should().notBeAnnotatedWith(RestController.class)
                    .andShould().beAnnotatedWith(FeignClient.class);

    @ArchTest
    public static final ArchRule feignClientItCanNotBeProtected =
            ArchRuleDefinition.classes()
                    .that().arePublic()
                    .and().areInterfaces()
                    .and().haveSimpleNameEndingWith("Controller")
                    .and().resideOutsideOfPackages(
                            "br.com.viasoft.rest.controller", "br.com.viasoft.integracaojetpdv.pdv.controller")
                    .should().notBeAnnotatedWith(Protected.class)
                    .andShould().beAnnotatedWith(FeignClient.class);

    @ArchTest
    public void feignClientMustHavePropertyNameFilled(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("FeignClient must have property name filled") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var feignClient = item.getAnnotationOfType(FeignClient.class);
                if (StringUtils.isBlank(feignClient.name())) {
                    events.add(SimpleConditionEvent.violated(item, "FeignClient annotation name property was not entered"));
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areInterfaces()
                .and().haveSimpleNameEndingWith("Controller")
                .and().resideOutsideOfPackages(
                        "br.com.viasoft.rest.controller", "br.com.viasoft.integracaojetpdv.pdv.controller")
                .should().beAnnotatedWith(FeignClient.class)
                .andShould(condition);

        rule.check(classes);
    }

    @ArchTest
    public void feignClientMustHavePropertyUrlFilled(JavaClasses classes) {
        var condition = new ArchCondition<JavaClass>("FeignClient must have property url filled") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                var feignClient = item.getAnnotationOfType(FeignClient.class);
                if (StringUtils.isBlank(feignClient.url())) {
                    events.add(SimpleConditionEvent.violated(item, item.getSimpleName() + " FeignClient annotation url property was not entered"));
                }
            }
        };

        var rule = ArchRuleDefinition.classes()
                .that().arePublic()
                .and().areInterfaces()
                .and().haveSimpleNameEndingWith("Controller")
                .and().resideOutsideOfPackages(
                        "br.com.viasoft.rest.controller", "br.com.viasoft.integracaojetpdv.pdv.controller")
                .should().beAnnotatedWith(FeignClient.class)
                .andShould(condition);

        rule.check(classes);
    }

}
