package br.com.viasoft.architecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class ApplicationListenerTest {

    @ArchTest
    public static final ArchRule applicationListenerShouldBeComponentOrService = ArchRuleDefinition.classes()
            .that().arePublic()
            .and().areAssignableTo(ApplicationListener.class)
            .should().beAnnotatedWith(Component.class)
            .orShould().beAnnotatedWith(Service.class);

}
