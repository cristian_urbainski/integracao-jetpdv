package br.com.viasoft.architecture;

import br.com.viasoft.security.annotation.Protected;
import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class ControllerTest {

    @ArchTest
    public static final ArchRule controllerImpl = ArchRuleDefinition.classes()
            .that().arePublic()
            .and().haveSimpleNameNotStartingWith("Pdv")
            .and().haveSimpleNameEndingWith("ControllerImpl")
            .and().doNotHaveModifier(JavaModifier.ABSTRACT)
            .should().beAnnotatedWith(Controller.class)
            .orShould().beAnnotatedWith(RestController.class);

    @ArchTest
    public static final ArchRule protectedImpl = ArchRuleDefinition.classes()
            .that().arePublic()
            .and().haveSimpleNameNotStartingWith("Pdv")
            .and().areAnnotatedWith(Controller.class)
            .or().areAnnotatedWith(RestController.class)
            .and().resideOutsideOfPackage("br.com.viasoft.rest.controller.impl")
            .should().beAnnotatedWith(Protected.class);

}
