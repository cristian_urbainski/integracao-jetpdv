package br.com.viasoft.architecture;

import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Repository;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
        packages = "br.com.viasoft.integracaojetpdv",
        importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeJars.class}
)
public class RepositoryTest {

    @ArchTest
    public static final ArchRule repositoryImpl = ArchRuleDefinition.classes()
            .that().arePublic()
            .and().haveSimpleNameNotStartingWith("Pdv")
            .and().haveSimpleNameEndingWith("RepositoryImpl")
            .and().doNotHaveModifier(JavaModifier.ABSTRACT)
            .should().beAnnotatedWith(Repository.class);

}
